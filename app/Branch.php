<?php

namespace App;

use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;


class Branch extends Model
{
	use Translatable;

    protected $translatable = ['title', 'address', 'orientir'];
}
