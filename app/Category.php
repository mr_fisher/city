<?php

namespace App;

use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;


class Category extends Model
{
	use Translatable;

    protected $translatable = ['title'];

    public function subCategory()
    {
    	return $this->hasMany('App\Subcategory');
    }
}
