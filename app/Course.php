<?php

namespace App;

use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{

   use Translatable;

   protected $translatable = ['title', 'desc', "duration_of_course", "number_of_lessons", "duration_of_lesson"];
    
}
