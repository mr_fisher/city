<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Gallery extends Model
{
	public function subCategory()
	{
		return $this->belongsTo('App\Subcategory');
	}
}
