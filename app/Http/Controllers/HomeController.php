<?php

namespace App\Http\Controllers;

use App\News;
use App\About;
use App\Review;
use App\Branch;
use App\Course;
use App\Gallery;
use App\Teacher;
use App\Partner;
use App\Category;
use App\Application;
use App\Statistic;
use App\Http\LocaleManager;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Translation;
use Illuminate\Support\Facades\Route;

class HomeController extends Controller
{
	public function index(Request $request, $locale = 'ru')
	{
		$routeName = Route::currentRouteName();

		$locale = LocaleManager::bindLocale($locale);

		if ($locale != 'ru' && $locale != 'uz') {
			return abort(404);
		} 

		$courses = Course::withTranslation($locale)->orderBy('order')->get();

		$teachers = Teacher::withTranslation($locale)->orderBy('order')->get();

		$news = News::withTranslation($locale)->orderBy('created_at', 'desc')->limit(4)->get();

		$reviews = Review::withTranslation($locale)->orderBy('order')->get();

		$branches = Branch::withTranslation($locale)->orderBy('id', 'desc')->get();

		$galery = Category::with(['subCategory' => function($q) use ($locale){
			$q->withTranslation($locale)->with('gallery');
		}])->withTranslation($locale)->get();

		$about = \App\About::withTranslation($locale)->first();

		$partners = Partner::orderBy('order')->get();

		$statistika = Statistic::first();


		return view('index')->with([
			'news' => $news,
			'about' => $about,
			'galery' => $galery,
			'locale' => $locale,
			'courses' => $courses,
			'reviews' => $reviews,
			'teachers' => $teachers,
			'branches' => $branches,
			'partners' => $partners,
			'statistika' => $statistika,
		]);
	}

	public function gallery(Request $request, $locale = 'ru', $id)
	{
		$routeName = Route::currentRouteName();

		$locale = LocaleManager::bindLocale($locale);

		if ($locale != 'ru' && $locale != 'uz') {
			return abort(404);
		} 

		$galery = Category::with(['subCategory' => function($q) use ($locale){
			$q->withTranslation($locale)->with('gallery');
		}])->withTranslation($locale)->find($id);

		return view('gallery')->with([
			'galery' => $galery,
			'locale' => $locale,
		]);
	}

	public function news(Request $request, $locale = 'ru')
	{
		$routeName = Route::currentRouteName();

		$locale = LocaleManager::bindLocale($locale);

		if ($locale != 'ru' && $locale != 'uz') {
			return abort(404);
		} 

		$news = News::withTranslation($locale)
							->orderBy('created_at', 'desc')
							->paginate(12);

		return view('news')->with([
			'locale' => $locale,
			'news' => $news,
		]);	
	}
	public function list(Request $request, $locale = 'ru')
	{
		$routeName = Route::currentRouteName();

		$locale = LocaleManager::bindLocale($locale);

		if ($locale != 'ru' && $locale != 'uz') {
			return abort(404);
		} 
		return view('list')->with([
			'locale' => $locale
		]);	
	}

	public function newsDetail(Request $request, $locale = 'ru', $url)
	{
		$routeName = Route::currentRouteName();

		$locale = LocaleManager::bindLocale($locale);

		if ($locale != 'ru' && $locale != 'uz') {
			return abort(404);
		} 

		$newsItem = News::withTranslation($locale)
							->where('url', $url)
							->first();

		$news = News::withTranslation($locale)
							->where('url', '!=' , $url)
							->orderBy('created_at', 'desc')
							->limit(4)
							->get();
		// dd($news);

		return view('news-detail')->with([
			'locale' => $locale,
			'newsItem' => $newsItem,
			'news' => $news,
		]);	
	}

	public function courseDetail(Request $request, $locale = 'ru', $url)
	{
		$routeName = Route::currentRouteName();

		$locale = LocaleManager::bindLocale($locale);

		if ($locale != 'ru' && $locale != 'uz') {
			return abort(404);
		} 

		$courseItem = Course::withTranslation($locale)
							->where('url', $url)
							->first();

		$courses = Course::withTranslation($locale)
							->where('url', '!=' , $url)
							->orderBy('created_at', 'desc')
							->limit(4)
							->get();
		// dd($news);

		return view('course-detail')->with([
			'locale' => $locale,
			'courseItem' => $courseItem,
			'courses' => $courses,
		]);	
	}
	public function registraciya(Request $request, $locale = 'ru', $url)
	{
		$routeName = Route::currentRouteName();

		$locale = LocaleManager::bindLocale($locale);

		if ($locale != 'ru' && $locale != 'uz') {
			return abort(404);
		} 

		$courseItem = Course::withTranslation($locale)
							->where('url', $url)
							->first();

		$courses = Course::withTranslation($locale)
							->where('url', '!=' , $url)
							->orderBy('created_at', 'desc')
							->limit(4)
							->get();
		// dd($news);

		return view('registraciya')->with([
			'locale' => $locale,
			'courseItem' => $courseItem,
			'courses' => $courses,
		]);	
	}

	public function book(Request $request)
	{
        // type 1  => kurslar
        // type 2  => callback

        $old_books = Application::all();

        foreach ($old_books as $old_book) {
        	$phone[] = $old_book->phone;
        }

        if (in_array($request->phone, $phone)) {
        	return 1;
        }

		if(isset($request->type) && $request->type == 1){
			$model = new Application;
			$model->first_name = $request->first_name;
			$model->last_name = $request->last_name;
			$model->type = "Курсы";
			$model->phone = $request->phone;
			$model->time = $request->time;
			$model->callback_type = $request->callback_type;
			$model->course = $request->course;
			if($model->save()){
				return 1;
			}
			return 0;
		}
		if(isset($request->type) && $request->type == 2){
			$model = new Application;
            $model->type = "Обратный звонок";
			$model->first_name = $request->first_name;
			$model->phone = $request->phone;
			if($model->save()){
			    return 1;
			}
			return 0;
		}
		if(isset($request->type) && $request->type == 3){
			$model = new Application;
            $model->type = "Оставить заявку";
			$model->first_name = $request->first_name;
			$model->phone = $request->phone;
			$model->message = $request->message;
			if($model->save()){
			    return 1;
			}
			return 0;
		}
	}

}
