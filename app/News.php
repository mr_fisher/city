<?php

namespace App;

use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;


class News extends Model
{

   use Translatable;

   protected $translatable = ['title', 'desc', 'short_desc'];
    
}
