<?php

namespace App;

use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;


class Subcategory extends Model
{
	use Translatable;

    protected $translatable = ['title'];

    public function category()
    {
    	return $this->belongsTo('App\Category');
    }

    public function gallery()
    {
    	return $this->hasMany('App\Gallery');
    }
}
