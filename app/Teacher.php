<?php

namespace App;

use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
	use Translatable;

    protected $translatable = ['name'];

    protected $table = 'teacher';
}
