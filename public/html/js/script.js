(function($){
    $(window).on("load",function(){
        $(document).scrollzipInit();
        $(document).rollerInit();
    });
    $(window).on("load scroll resize", function(){
        $('.numscroller').scrollzip({
            showFunction    :   function() {
                                    numberRoller($(this).attr('data-slno'));
                                },
            wholeVisible    :     false,
        });
    });
    $.fn.scrollzipInit=function(){
        $('body').prepend("<div style='position:fixed;top:0px;left:0px;width:0;height:0;' id='scrollzipPoint'></div>" );
    };
    $.fn.rollerInit=function(){
        var i=0;
        $('.numscroller').each(function() {
            i++;
           $(this).attr('data-slno',i); 
           $(this).addClass("roller-title-number-"+i);
        });        
    };
    $.fn.scrollzip = function(options){
        var settings = $.extend({
            showFunction    : null,
            hideFunction    : null,
            showShift       : 0,
            wholeVisible    : false,
            hideShift       : 0,
        }, options);
        return this.each(function(i,obj){
            $(this).addClass('scrollzip');
            if ( $.isFunction( settings.showFunction ) ){
                if(
                    !$(this).hasClass('isShown')&&
                    ($(window).outerHeight()+$('#scrollzipPoint').offset().top-settings.showShift)>($(this).offset().top+((settings.wholeVisible)?$(this).outerHeight():0))&&
                    ($('#scrollzipPoint').offset().top+((settings.wholeVisible)?$(this).outerHeight():0))<($(this).outerHeight()+$(this).offset().top-settings.showShift)
                ){
                    $(this).addClass('isShown');
                    settings.showFunction.call( this );
                }
            }
            if ( $.isFunction( settings.hideFunction ) ){
                if(
                    $(this).hasClass('isShown')&&
                    (($(window).outerHeight()+$('#scrollzipPoint').offset().top-settings.hideShift)<($(this).offset().top+((settings.wholeVisible)?$(this).outerHeight():0))||
                    ($('#scrollzipPoint').offset().top+((settings.wholeVisible)?$(this).outerHeight():0))>($(this).outerHeight()+$(this).offset().top-settings.hideShift))
                ){
                    $(this).removeClass('isShown');
                    settings.hideFunction.call( this );
                }
            }
            return this;
        });
    };
    function numberRoller(slno){
            var min=$('.roller-title-number-'+slno).attr('data-min');
            var max=$('.roller-title-number-'+slno).attr('data-max');
            var timediff=$('.roller-title-number-'+slno).attr('data-delay');
            var increment=$('.roller-title-number-'+slno).attr('data-increment');
            var numdiff=max-min;
            var timeout=(timediff*1000)/numdiff;
            //if(numinc<10){
                //increment=Math.floor((timediff*1000)/10);
            //}//alert(increment);
            numberRoll(slno,min,max,increment,timeout);
            
    }
    function numberRoll(slno,min,max,increment,timeout){//alert(slno+"="+min+"="+max+"="+increment+"="+timeout);
        if(min<=max){
            $('.roller-title-number-'+slno).html(min);
            min=parseInt(min)+parseInt(increment);
            setTimeout(function(){numberRoll(eval(slno),eval(min),eval(max),eval(increment),eval(timeout))},timeout);
        }else{
            $('.roller-title-number-'+slno).html(max);
        }
    }
})(jQuery);
jQuery(document).ready(function($) {    
    Date.prototype.toDateInputValue = (function() {
        var local = new Date(this);
        local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
        return local.toJSON().slice(0,10);
    });
    $('.review_date').val(new Date().toDateInputValue());

    $('.send_review a').click(function(event) {
        $('.review_modal').fadeIn(200);
    });
    $('input[name=phone]').mask('000-00-0000000');

    $('.slider_row').flickity({
        wrapAround: true,
        pageDots: false,
        autoPlay: 4000
    });
    $('.video_slider').flickity({
        wrapAround: true,
        groupCells: '1',
        adaptiveHeight: true,
        pageDots: false
    });
    $(window).scroll(function(){
      if ($(window).scrollTop() >= 300) {
        $('.second_header').addClass('top');
      } else if($(window).scrollTop() <= 300) {
        $('.second_header').removeClass('top');
      }
    });
    
    $('.slider_block a.call,.slider_block a.question').click(function(event) {
        $('.callback_modal').fadeIn(200);
    });
    $('.slider_block a.lesson').click(function(event) {
        $('.free_lesson').fadeIn(200);
    });
    $('.slider_block a.question').click(function(event) {
        $('.modal select').val('Задать вопрос');
        $('.textarea_block').show();
    });
    
    $('.gallery_carousel').flickity({
        wrapAround: true
    });
    $('.header_btn a,header .second_header .menu_block .menu a.btn').click(function(event) {
        $('.textarea_block').hide();
        $('.modal select').val('Обратный звонок');
        $('.callback_modal').fadeIn(200);
    });
    $('.close_modal').click(function(event) {
      $('.modal').not('.gallery_modal').fadeOut(200);
    });
    $('.close_icon').click(function(event) {
      $('.menu_block').fadeOut(200);
        $('body').css('overflow', 'unset');
    });

    $('.service_item a, .courses_list a').click(function(event) {
        $('.contacts_modal').fadeIn(200);
        var form_title = $(this).parent('.btn_block').parent('.courses_row').siblings('h4').text();
        $('.contacts_modal h4').text(form_title);
        $('.contacts_modal input[name=category]').val(form_title);
    });
    $('.courses_list a').click(function(event) {
        $('.contacts_modal').fadeIn(200);
        var form_title2 = $(this).text();
        $('.contacts_modal h4').text(form_title2);
        $('.contacts_modal input[name=category]').val(form_title2);
    });
    $('.gallery_section .gallery_category').click(function(event) {
        $('.gallery_modal[data-gallery=' + $(this).data("gallery-open") + ']').addClass('open');
    });
    $('.gallery_modal .gallery_category a').click(function(event) {
        $('.gallery_carousel').removeClass('active');
        $('.gallery_carousel[data-slider=' + $(this).data("slider-open") + ']').addClass('active');
    });
    /* $('.service_item a:first-child').click(function(event) {
        var title = $(this).parent('.btn_block').siblings('h4').text();
        var text = $(this).parent('.btn_block').siblings('p').text();
        $('.courses_modal').fadeIn(200);
        $('.courses_modal h4').html(title);
        $('.courses_modal p').html(text);
    });*/
    $('.gallery_modal .close_modal').click(function(event) {
        $('.gallery_modal').removeClass('open');
    });
    $('.contact_call').click(function(event) {
        $('.callback_modal').fadeIn(200);
    });
    $('.menu_icon').click(function(event) {
        $('.menu_block').fadeIn(200);
        $('body').css('overflow', 'hidden');
    });
    $(document).on('click', 'a[href^="#"]', function (event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 80
        }, 500);
    });
    var offset = parseInt($('section').offset().top);
    var bheight = $(window).height();
    var percent = 0.5;
    var hpercent = bheight * percent;

    var sections = $('section')
    , nav = $('header .menu')

    $(window).on('scroll', function () {
    var cur_pos = $(this).scrollTop();

        sections.each(function() {
          var top = $(this).offset().top - hpercent,
              bottom = top + $(this).outerHeight();

          if (cur_pos >= top && cur_pos <= bottom) {
            nav.find('a').removeClass('active');
            sections.removeClass('active');

            $(this).addClass('active');
            nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
          }
        });
    });
    $('.open_form').click(function(event) {
        $('.corporate_modal').fadeIn(200);
    });
    
    if($(window).width() < 576){
        $('.gallery_modal .gallery_category').flickity({
            wrapAround: true,
            pageDots: false,
            cellAlign: 'left',
            contain: true
        });
        
    }else if($(window).width() < 992){
        $('header .second_header .menu_block .menu a').click(function(event) {
            $('.menu_block').fadeOut(200);
        });
    }
    $('.courses_section .row').flickity({
        wrapAround: true,
        pageDots: false,
        groupCells: true,
        freeScroll: true,
        adaptiveHeight: true
    });
    $('.features_slider').flipbox({
        vertical: true
    });
    $('button.prev').click(function() {
        $('.features_slider').flipbox('prev', $(this).hasClass('reverse'));
    });
    $('button.next').click(function() {
        $('.features_slider').flipbox('next', $(this).hasClass('reverse'));
    });


});