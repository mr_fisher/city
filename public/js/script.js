(function($){
    $(window).on("load",function(){
        $(document).scrollzipInit();
        $(document).rollerInit();
    });
    $(window).on("load scroll resize", function(){
        $('.numscroller').scrollzip({
            showFunction    :   function() {
                                    numberRoller($(this).attr('data-slno'));
                                },
            wholeVisible    :     false,
        });
    });
    $('form').on('submit',  function(){
        ym(52054707, 'reachGoal', 'form_name');
        return true;
    })
    $('.about_text a').click(function(){
        $('.about_text .about_text_block').toggleClass('active');
        $('.teachers_row').slideToggle(200);
        if($(this).text() == 'Подробнее'){
            $(this).text('Меньше');
        }else if($(this).text() == 'Показать меньше'){
            $(this).text('Подробнее');
        }else if($(this).text() == 'Batafsil'){
            $(this).text('Yopish');
        }else if($(this).text() == 'Yopish'){
            $(this).text('Batafsil');
        }
    });
    $.fn.scrollzipInit=function(){
        $('body').prepend("<div style='position:fixed;top:0px;left:0px;width:0;height:0;' id='scrollzipPoint'></div>" );
    };
    $.fn.rollerInit=function(){
        var i=0;
        $('.numscroller').each(function() {
            i++;
           $(this).attr('data-slno',i); 
           $(this).addClass("roller-title-number-"+i);
        });        
    };
    $.fn.scrollzip = function(options){
        var settings = $.extend({
            showFunction    : null,
            hideFunction    : null,
            showShift       : 0,
            wholeVisible    : false,
            hideShift       : 0,
        }, options);
        return this.each(function(i,obj){
            $(this).addClass('scrollzip');
            if ( $.isFunction( settings.showFunction ) ){
                if(
                    !$(this).hasClass('isShown')&&
                    ($(window).outerHeight()+$('#scrollzipPoint').offset().top-settings.showShift)>($(this).offset().top+((settings.wholeVisible)?$(this).outerHeight():0))&&
                    ($('#scrollzipPoint').offset().top+((settings.wholeVisible)?$(this).outerHeight():0))<($(this).outerHeight()+$(this).offset().top-settings.showShift)
                ){
                    $(this).addClass('isShown');
                    settings.showFunction.call( this );
                }
            }
            if ( $.isFunction( settings.hideFunction ) ){
                if(
                    $(this).hasClass('isShown')&&
                    (($(window).outerHeight()+$('#scrollzipPoint').offset().top-settings.hideShift)<($(this).offset().top+((settings.wholeVisible)?$(this).outerHeight():0))||
                    ($('#scrollzipPoint').offset().top+((settings.wholeVisible)?$(this).outerHeight():0))>($(this).outerHeight()+$(this).offset().top-settings.hideShift))
                ){
                    $(this).removeClass('isShown');
                    settings.hideFunction.call( this );
                }
            }
            return this;
        });
    };
    function numberRoller(slno){
            var min=$('.roller-title-number-'+slno).attr('data-min');
            var max=$('.roller-title-number-'+slno).attr('data-max');
            var timediff=$('.roller-title-number-'+slno).attr('data-delay');
            var increment=$('.roller-title-number-'+slno).attr('data-increment');
            var numdiff=max-min;
            var timeout=(timediff*1000)/numdiff;
            //if(numinc<10){
                //increment=Math.floor((timediff*1000)/10);
            //}//alert(increment);
            numberRoll(slno,min,max,increment,timeout);
            
    }
    function numberRoll(slno,min,max,increment,timeout){//alert(slno+"="+min+"="+max+"="+increment+"="+timeout);
        if(min<=max){
            $('.roller-title-number-'+slno).html(min);
            min=parseInt(min)+parseInt(increment);
            setTimeout(function(){numberRoll(eval(slno),eval(min),eval(max),eval(increment),eval(timeout))},timeout);
        }else{
            $('.roller-title-number-'+slno).html(max);
        }
    }
})(jQuery);
jQuery(document).ready(function($) {  
    if($('.course_title').text() == 'Курсы английского языка для детей' || $('.course_title').text() == 'Ingliz tili bolalar uchun'){
        $('input[name=time]:first-child').val('10:00');
        $('.time_block').hide();
    }else if($('.course_title').text() == 'IELTS за 30 дней!' || $('.course_title').text() == 'IELTS 30 kunda!' || $('.course_title').text() == 'Разговорный английский за 30 дней!' || $('.course_title').text() == '30 kunda Ingliz tilida gaplashing!'){
        $('.time_block .radio_18').hide();
        $('.time_block .radio_12').hide();
    }else{
        $('input[name=time]:first-child').val('09:00');
        $('.time_block').show();
    }
    if($('.course_title').text() == '30 kunda Ingliz tilida gaplashing!'){
        $('.course_section .course_img img').attr('src', 'http://city.uz/img/speaking_uzb.png');
    }
    $('a#form').trigger('click');  
    Date.prototype.toDateInputValue = (function() {
        var local = new Date(this);
        local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
        return local.toJSON().slice(0,10);
    });
    $('.review_date').val(new Date().toDateInputValue());

    $('.send_review a').click(function(event) {
        $('.review_modal').fadeIn(200);
    });
    $('input[name=phone]').mask('000-00-0000000');
    $('.slider_row').flickity({
        wrapAround: true,
        pageDots: false,
        autoPlay: 4000
    });
    $('.main_course .open_form').click(function(){
        $('input[name=course]').val("Английский за 3 месяца");
    });
    $('.lesson').click(function(){
        $('.main_lesson_modal').fadeIn(200);
    });
    $('.video_slider').flickity({
        wrapAround: true,
        groupCells: '1',
        adaptiveHeight: true,
        pageDots: false
    });
    $(window).scroll(function(){
      if ($(window).scrollTop() >= 300) {
        $('.second_header').addClass('top');
      } else if($(window).scrollTop() <= 300) {
        $('.second_header').removeClass('top');
      }
    });
    
    $('.slider_block a.call,.slider_block a.question').click(function(event) {
        $('.zayavka_modal').fadeIn(200);
    });
    $('.button_block a.open_form').click(function(event) {
        $('.sign_in_modal').fadeIn(200);
    });
    $('.slider_block a.question').click(function(event) {
        $('.modal select').val('Задать вопрос');
        $('.textarea_block').show();
    });
    $('.course_section .btns a').click(function(){
        $('.modal.sign_in_modal').fadeIn(200);
        $('input[name=course]').val($('.course_title').text());
        if($('.course_title').text() == 'Курсы английского языка для детей' || $('.course_title').text() == 'Ingliz tili bolalar uchun'){
            $('input[name=time]:first-child').val('10:00');
            $('.time_block').hide();
        }else if($('.course_title').text() == 'IELTS за 30 дней!' || $('.course_title').text() == 'IELTS 30 kunda!' || $('.course_title').text() == 'Разговорный английский за 30 дней!' || $('.course_title').text() == '30 kunda ingliz tilida gaplashing!'){
            $('.time_block .radio_18').hide();
            $('.time_block .radio_12').hide();
        }else{
            $('input[name=time]:first-child').val('09:00');
            $('.time_block').show();
        }
    });

    $('.gallery_item_section .col-md-6 .gallery_item_img').click(function(){
        $('.img_modal').fadeToggle(200);
        $('.img_modal .modal_content img').attr('src', $(this).children('img').attr('src'));
    });
    $('.gallery_carousel').flickity({
        wrapAround: true
    });
    $('.header_btn a,header .second_header .menu_block .menu a.btn').click(function(event) {
        $('.textarea_block').hide();
        $('.modal select').val('Обратный звонок');
        $('.callback_modal').fadeIn(200);
    });
    $('.close_modal').click(function(event) {
      $('.modal').not('.gallery_modal').fadeOut(200);
    });
    $('.close_icon').click(function(event) {
      $('.menu_block').fadeOut(200);
        $('body').css('overflow', 'unset');
    });

    $('.service_item a:first-child, .courses_list a').click(function(event) {
        var form_title = $(this).parent('.btn_block').parent('.courses_row').siblings('h4').children('span').text();
        if(form_title == 'Курсы английского языка для детей' || form_title == 'Ingliz tili bolalar uchun' ){
            $('input[name=time]:first-child').val('10:00');
            $('.time_block').hide();
        }else if(form_title == 'IELTS за 30 дней!' || form_title == 'IELTS 30 kunda!' || form_title == 'Разговорный английский за 30 дней!' || form_title == '30 kunda Ingliz tilida gaplashing!'){
            $('.time_block .radio_18').hide();
            $('.time_block .radio_12').hide();
        }else{
            $('input[name=time]:first-child').val('09:00');
            $('.time_block .radio_18').show();
            $('.time_block .radio_12').show();
            $('.time_block').show();
        }
        $('.contacts_modal').fadeIn(200);
        $('.contacts_modal h4').text(form_title);
        $('.contacts_modal input[name=course]').val(form_title);
    });
    $('.courses_list a').click(function(event) {
        $('.contacts_modal').fadeIn(200);
        var form_title2 = $(this).text();
        $('.contacts_modal h4').text(form_title2);
        $('.contacts_modal input[name=category]').val(form_title2);
    });
    $('.gallery_section .gallery_category').click(function(event) {
        $('.gallery_modal[data-gallery=' + $(this).data("gallery-open") + ']').addClass('open');
    });
    $('.filter_btns a').click(function(event) {
        $('.gallery_row').hide();
        $('.gallery_row[data-gallery='+ $(this).data("gallery-open") + ']').fadeIn(200);
    });
    $('.filter_btns a').click(function(event) {
        $('.gallery_carousel').removeClass('active');
        $('.gallery_carousel[data-slider=' + $(this).data("slider-open") + ']').addClass('active');
    });
    $('.contest_btn').click(function(){
        $('.contest_desc').hide();
        $('.contest_form').fadeIn(300);
    });
    $('.back_btn').click(function(){
        $('.contest_form').hide();
        $('.contest_desc').fadeIn(300);
    });
    $('.gallery_modal .close_modal').click(function(event) {
        $('.gallery_modal').removeClass('open');
    });
    $('.contact_call').click(function(event) {
        $('.callback_modal').fadeIn(200);
    });
    $('.menu_icon').click(function(event) {
        $('.menu_block').fadeIn(200);
        $('body').css('overflow', 'hidden');
    });
    $(document).on('click', 'a[href^="#"]', function (event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 80
        }, 500);
    });
    var offset = parseInt($('section').offset().top);
    var bheight = $(window).height();
    var percent = 0.5;
    var hpercent = bheight * percent;

    var sections = $('section')
    , nav = $('header .menu')

    $(window).on('scroll', function () {
    var cur_pos = $(this).scrollTop();

        sections.each(function() {
          var top = $(this).offset().top - hpercent,
              bottom = top + $(this).outerHeight();

          if (cur_pos >= top && cur_pos <= bottom) {
            nav.find('a').removeClass('active');
            sections.removeClass('active');

            $(this).addClass('active');
            nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
          }
        });
    });
    $('.slider .large').click(function(event) {
        $('.sign_in_modal').fadeIn(200);
    });
    $(".sign_in_modal .container").click(function() {
        $(this).parent('.input_block').children('.container').children('input').removeAttr('checked');
        $(this).children('input').attr('checked', true);
        
    });
    if($(window).width() < 576){
        $('.gallery_modal .gallery_category').flickity({
            wrapAround: true,
            pageDots: false,
            cellAlign: 'left',
            contain: true
        });
        $('.courses_section .service_item').click(function(){
            $(this).children('.item_content').children('.courses_row').slideToggle(200);
        });
        $('header .second_header .menu_block .menu a').click(function(event) {
            $('.header .second_header .menu_block').fadeOut(300);
        });
        
    }else if($(window).width() < 992){
        $('header .second_header .menu_block .menu a').click(function(event) {
            $('.menu_block').fadeOut(200);
        });
    }
    if($(window).width() > 575){
        $('.courses_section .row').flickity({
            wrapAround: true,
            pageDots: false,
            groupCells: true,
            freeScroll: true,
            adaptiveHeight: true,
            cellAlign: 'left'
        });
    }
    
    $('.features_slider').flipbox({
        vertical: true
    });
    $('button.prev').click(function() {
        $('.features_slider').flipbox('prev', $(this).hasClass('reverse'));
    });
    $('button.next').click(function() {
        $('.features_slider').flipbox('next', $(this).hasClass('reverse'));
    });
    $('.news_slider').flickity({
        wrapAround: true,
        pageDots: false,
        adaptiveHeight: true,
        contain: true,
        cellAlign: 'center'
    });
    $('.accordion_item a').click(function(e) {
      e.preventDefault();                        
      
      var $this = $(this),
          item = $this.closest('.faq_accordion_item'), 
          content = item.find('.faq_item_content'),  
          contents = $('.faq_item_content');     
      
      if(!item.hasClass('open')){ 
        contents.slideUp(500);                     
        item.addClass('open').siblings().removeClass('open');
        content.slideDown(500);             
      } else { 
        item.removeClass('open');
        contents.slideUp(500);
      }
      
    });
});
var Conclave=(function(){
    var buArr =[],arlen;
    return {
        init:function(){
            this.addCN();this.clickReg();
        },
        addCN:function(){
            var buarr=["holder_bu_awayL2","holder_bu_awayL1","holder_bu_center","holder_bu_awayR1","holder_bu_awayR2"];
            for(var i=1;i<=buarr.length;++i){
                $("#bu"+i).removeClass().addClass(buarr[i-1]+" holder_bu");
            }
        },
        clickReg:function(){
            $(".holder_bu").each(function(){
                buArr.push($(this).attr('class'))
            });
            arlen=buArr.length;
            for(var i=0;i<arlen;++i){
                buArr[i]=buArr[i].replace(" holder_bu","")
            };
            $(".holder_bu").click(function(buid){
                var me=this,id=this.id||buid,joId=$("#"+id),joCN=joId.attr("class").replace(" holder_bu","");
                var cpos=buArr.indexOf(joCN),mpos=buArr.indexOf("holder_bu_center");
                if(cpos!=mpos){
                    tomove=cpos>mpos?arlen-cpos+mpos:mpos-cpos;
                    while(tomove){
                        var t=buArr.shift();
                        buArr.push(t);
                        for(var i=1;i<=arlen;++i){
                            $("#bu"+i).removeClass().addClass(buArr[i-1]+" holder_bu");
                        }
                        --tomove;
                    }
                }
            })
        },
        auto:function(){
            for(i=1;i<=1;++i){
                $(".holder_bu").delay(4000).trigger('click',"bu"+i).delay(4000);
                console.log("called");
            }
        }
    };
})();
$('.flickity-prev-next-button.next').click(function(){
    if($('.testimonials_row input[name=slider]').is(':checked')) {
        console.log('TEST');
        $('.testimonials_row input[name=slider]:checked').next("input").prop('checked', 'true');
        $('.testimonials_row input[name=slider]:checked').removeAttr('checked');

    }
});

if($('.testimonials_row #s5').is(':checked')){
    $('.flickity-prev-next-button.next').click(function(){
        $('.testimonials_row input[name=slider]').prop('checked', 'false');
        $('.testimonials_row input[name=slider].#s1').prop('checked', 'true');
    });
}
$('.flickity-prev-next-button.previous').click(function(){
    if($('.testimonials_row input[name=slider]').is(':checked')) {
        $('.testimonials_row input[name=slider]:checked').prev("input").prop('checked', 'true');
    }

});
$('.show_more_btn a').click(function(){
    $('.teachers_row').slideToggle(200);
    if($(this).text() == 'Открыть'){
        $(this).text('Закрыть');
    }else if($(this).text() == 'Закрыть'){
        $(this).text('Открыть');
    }else if($(this).text() == 'Ochish'){
        $(this).text('Yopish');
    }else if($(this).text() == 'Yopish'){
        $(this).text('Ochish');
    }
});