$(window).on('load', function () {
    $('.preloader').delay(150).fadeOut('slow');
});
jQuery(document).ready(function($) {

	$("#one-scroll").onepage_scroll({
	   sectionContainer: "section",     
	   easing: "ease",                                   
	   animationTime: 700,             
	   pagination: true,                
	   updateURL: false,                
	   beforeMove: function(index) {},  
	   afterMove: function(index) {},   
	   loop: true,                    
	   keyboard: true,                 
	   responsiveFallback: false,        
	   direction: "vertical"           
	});
	$( ".main_section" ).keydown(function() {
	    $(".social_icons, .phone_block").css({
            'opacity': '0',
            'z-index': '-1'
        });
	});
    $( "body" ).keyup(function() {
        $(".social_icons, .phone_block").css({
            'opacity': '0',
            'z-index': '-1'
        });
    });
	$('.slider_row').flickity({
		contain : true,
		groupCells: true,
        pageDots: false,
        prevNextButtons: false
	});
	$('.services_section .arrow.prev').on( 'click', function() {
        $('.services_slider').flickity('previous');
    });
    $('.services_section .arrow.next').on( 'click', function() {
        $('.services_slider').flickity('next');
    });
	$('.portfolio_section .arrow.prev').on( 'click', function() {
        $('.slider_row').flickity('previous');
    });
    $('.portfolio_section .arrow.next').on( 'click', function() {
        $('.slider_row').flickity('next');
    });
	$('.input_block p').click(function(event) {
        $(this).siblings('input').focus();
        $(this).siblings('textarea').focus();
    });
    $(' form input,  form textarea').focus(function(event) {
        $(this).parent('.input_block').children('p').addClass('focus');
        $(this).addClass('input_focus');
    });
    $('.services_slider').flickity({
    	pageDots: false,
    	prevNextButtons: false,
    	wrapAround: true
    });
    $('#one-scroll').bind('mousewheel', function(e){
        if ($(".services_section").hasClass("active")) {
            $(".social_icons, .phone_block").css({
                'opacity': '0',
                'z-index': '-1'
            });
        }else{
            $(".social_icons, .phone_block").css({
                'opacity': '1',
                'z-index': '2'
            });
        }
        if ($(".contacts_section").hasClass("active")) {
          $(".phone_block").addClass('black');
        }else{
          $(".phone_block").removeClass('black');
        }
    });
    $('.onepage-pagination').bind('click', function(e){
        if ($(".services_section").hasClass("active")) {
            $(".social_icons, .phone_block").css({
                'opacity': '0',
                'z-index': '-1'
            });
        }else{
            $(".social_icons, .phone_block").css({
                'opacity': '1',
                'z-index': '2'
            });
        }
        if ($(".contacts_section").hasClass("active")) {
          $(".phone_block").addClass('black');
        }else{
          $(".phone_block").removeClass('black');
        }
    });
    $('.menu_list_block a, .main_section .btn a, .buttons_block a').click(function(event) {
        $('#one-scroll').moveTo($(this).data('scroll'));
    });
    
    
    $('form input, form textarea').blur(function(event) {
        if($(this).val().length === 0 ){
            $(this).parent('.input_block').children('p').removeClass('focus');
            $(this).removeClass('input_focus');
        }
        
    });
    $('.menu_icon').click(function(){
    	$(this).toggleClass('active');
    	$('.menu_list').toggleClass('active');
    });
    $('.portfolio_section .btn_block a').click(function(){
        
        $('.slider_row').removeClass('active');
        $('.slider_row[data-tab=' + $(this).data("tab-open") + ']').fadeIn(200).addClass('active');
    });
    if($(window).width() < 768 && $(window).width() > 575) {
        
        $(window).resize(function(){
            $('.services_section .services_slider .slider_item .slider_content').each(function() {
                $(this).height($(this).width());
            });

        }).resize();
    }
    if($(window).width() < 575) {  
        $('.portfolio_section .btn_block').flickity({
            prevNextButtons: false,
            pageDots: false,
            wrapAround: true,
            groupCells: true
        });
    }
    $('.menu_btn a').click(function(){
        $('.modal').fadeIn(200).addClass('active');
    });
    $('.close').click(function(){
        $('.modal').fadeOut(200).removeClass('active');
    });

	
});