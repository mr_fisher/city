<?php

return [
    'rollin_name' => 'Ваше имя',
    'rollin_surname' => 'Ваша фамилия',
    'rollin_number' => 'Ваш номер телефона',
    'rollin_call' => 'Позвонить',
    'type_callback' => "Как связаться с вами?",
    'rollin_telegram' => 'Написать в telegram',
    'rollin_region' => 'Ваш регион',
    'slider_meta' => 'Наш основной курс',
    'rollin_time' => 'Удобное для вас время',
    'courses_link' => '<div class="courses_link">
                <h4>Наши остальные курсы:</h4>
                <a href="http://city.uz/ru/course/english-in-three-months">Английский язык за 3 месяцa</a>
                <a href="http://city.uz/ru/course/ingliz-tili-olti-oyda">Английский язык за 6 месяцев</a>
                <a href="http://city.uz/ru/course/ielts-in-month">IELTS за 30 дней</a>
                <a href="http://city.uz/ru/course/ielts">IELTS > 7.0 за 3 месяца</a>
                <a href="http://city.uz/ru/course/city-speaking">Разговорные курсы за 3 месяца</a>
                <a href="http://city.uz/ru/course/speak">Разговорный английский за месяц</a>
                <a href="http://city.uz/ru/course/sunday-kids">Курсы английского языка для детей</a>
            </div>',
	'courses_duration' => 'Продолжительность',
    'courses_week' => 'Кол-во занятий в неделю',
    'courses_time' => 'Длительность урока',
    'about' => 'О нас',
    'courses' => 'Курсы',
    'open' => 'Открыть',
    'region' => "Ташкент",
    'region2' => "Ташкентская область",
    'region3' => "Андижанская область",
    'region4' => "Бухарская область",
    'region5' => "Джизакская область",
    'region6' => "Кашкадарьинская область",
    'region7' => "Навоийская область",
    'region8' => "Наманганская область",
    'region9' => "Самаркандская область",
    'region10' => "Сурхандарьинская область",
    'region11' => "Сырдарьинская область",
    'region12' => "Ферганская область",
    'region13' => "Хорезмская область",
    'region14' => "Республика Каракалпакистан",
    'courses_english3' => 'Английский за 3 месяца',
    'courses_english6' => 'Английский за 6 месяцев',
    'courses_corporate' => 'Корпоративное обучение',
    'statistycs' => 'Учебный центр в цифрах',
    'gallery' => 'Галерея',
    'rollin' => "Записаться на курс",
    'rollin_title' => "Оставьте заявку на пробный урок!",
    'back' => "Вернуться на главную страницу",
    'testimonials' => 'Отзывы',
    'news' => 'Новости',
    'readmore' => "Подробнее",
    'filial' => 'Филиалы',
    'contacts' => 'Контакты',
    'main' => 'Главная',
    'partners_title' => 'Наши партнеры',
    'address' => 'Адрес',
    'orientir' => 'Ориентир',
    'courses_subtitle' => 'Готовы учиться <br>по ускоренной программе?<br><span>Мы предлагаем:</span>',
    'courses_title' => 'Наши курсы',
    'signup' => 'Записаться на курс',
    'callback' => 'Обратный звонок',
    'slider_title' => 'Курсы английского языка в Ташкенте!',
    'slider_title2' => 'Учебный центр английского языка в ташкенте!',
    'slider_title3' => 'Английский язык за 3 месяца',
    'slider_title4' => 'Разговорный английский за<br>30 дней!',
    'slider_title5' => 'IELTS за 30 дней!',
    'slider1_subtitle1' => 'Записать Вас на <br>пробный урок?',
    'slider1_subtitle2' => 'Есть вопросы?',
    'slider2_subtitle' => 'City Education - учебный центр английского языка в Ташкенте с обширным выбором курсов: английский язык за 3 и 6 месяцев, подготовка к IELTS, курсы разговорного английского, а также курсы английского для детей. Помимо этого мы также запустили ускоренные курсы обучения английскому языку! ',
    'slider3_subtitle' => 'Данный курс английского языка нацелен на изучение грамматики, развитие разговорной речи, чтение. Урок длится три часа, преподают три учителя, на четвертом часу занятий к учащимся заходит четвертый преподаватель, который помогает учащимся освоить пройденный материал.',
    'slider4_subtitle' => 'Хотите свободно говорить на английском языке? Специальное предложение для вас и для тех, кто ходил на курсы английского языка, но всё ещё не умеет говорить! Учебный центр “City Education” запустил в Ташкенте ускоренный интенсивный курс – “Разговорный английский за 30 дней”!',
    'slider5_subtitle' => 'Хотите получить высокий балл на экзамене IELTS в короткие сроки? Мало время, чтобы получить баллы для поступления в университеты? Специальное предложение для вас! Учебный центр “City Education” запустил ускоренный интенсивный курс – IELTS за 30 дней”!',
    'result_text1' => 'с <span>2013</span> года',
    'result_text2' => 'более <span>8000</span> учащихся',
    'result_text3' => 'до <span>2019</span> года!',
    'slider1_btn1' => 'Да, записать',
    'slider1_btn2' => 'Да, есть вопрос',
    'slider2_btn1' => 'Скачать коммерческое предложение',
    'slider2_btn2' => 'Зарегистрируйтесь!',
    'slider4_btn' => "<a href='http://city.uz/ru/course/speak'>Подробнее</a>",
    'slider5_btn' => "<a href='http://city.uz/ru/course/ielts-in-month'>Подробнее</a>",
    'teachers_title' => "Наши учителя",
    'teachers_subtitle' => "С нами даже картошка заговорит на английском!",
    'features_title' => "Мы сильно отличаемся от других",
    'features_subtitle' => "Все хотят быть лучшими, а мы уже самые лучшие!",
    'features_box_title' => "Мы гарантируем",
    'features_box_title2' => "Наши обещания",
    'features_box_title3' => "Почему именно мы?",
    'features_box_title4' => "Наши преимущества",
    'features_box_text' => 'За оплаченную вами сумму вы получите гарантийное письмо!',
    'features_box_text2' => 'Вернем деньги, если вы не достигнете результата!',
    'features_box_text3' => 'Постоянный анализ и контроль качества обучения и успеваемости!',
    'features2_box_text' => 'Уникальная и современная система!',
    'features2_box_text2' => 'Комфортная и интересная атмосфера!',
    'features2_box_text3' => 'Приятная и удобная обстановка!',
    'features3_box_text' => 'Предоставим максимально эффективное обучение!',
    'features3_box_text2' => 'Создадим лучший образовательный сервис!',
    'features3_box_text3' => 'Всегда и везде держим планку на высоком уровне!',
    'features4_box_text' => 'Всегда поддерживаем прочные отношения!',
    'features4_box_text2' => 'Решаем возникшие вопросы в кратчайшие сроки!',
    'features4_box_text3' => 'Никогда не подведем и сдержим слово!',
    'statistycs_subtitle' => 'которая говорит о нас',
    'statistycs_item' => 'учеников обучаются у нас ',
    'statistycs_item2' => 'получили сертификат IELTS',
    'statistycs_item3' => 'более 10 опытных преподавателей',
    'statistycs_item4' => 'филиала по Ташкенту',
    'gallery_title' => 'Галерея',
    'gallery_subtitle' => 'Фотографии с мероприятий',
    'news_title' => 'Полезные статьи и новости об учебном центре!',
    'news_subtitle' => 'Изучайте английский язык онлайн вместе с нами!',
    'testimonials_title' => 'Отзывы',
    'testimonials_subtitle' => 'Отзывы наших учеников',
    'filial_title' => 'Филиалы',
    'filial_subtitle' => 'Наши контактные данные',
    'testimonials_btn' => 'Оставить отзыв',
    'more_news_title' => 'Вам также может быть интересно:',
    'more' => 'Больше статей',
    'form_name' => 'Ваше имя',
    'form_number' => 'Ваш номер телефона',
    'form_mail' => 'Ваш E-mail',
    'form_msg' => 'Ваше сообщение',
    'form_title' => 'Оставить заявку',
    'form_title2' => 'Пробный урок',
    'form_title3' => 'Корпоративное обучение',
    'form_title4' => 'Оставить отзыв',
    'send_btn' => 'Отправить',
    'form_date' => 'Дата',
    'form_status' => 'Ваш статус',
    'student' => 'Учащийся',
    'teacher' => 'Учитель',
    'parent' => 'Родитель',
    'company_name' => 'Название вашей фирмы',
    'pupils_amount' => 'Количество учащихся',
    'lessons_amount' => 'Кол-во занятий в неделю',
    'faq_title' => 'часто задаваемые вопросы',
    'faq_list' => '<div class="accordion_list">          
                    <div class="accordion_item faq_accordion_item">
                        <div class="accordion_button">
                          <a href="#">
                            <span>Когда можно прийти на пробный урок?</span>
                            <div class="arrow_bottom">
                                <img src="img/arrow_down.svg">
                            </div>
                          </a>

                        </div>
                        <div class="faq_item_content item_content">
                            <div class="item_content_block">
                                <p>Список проведения пробных уроков:</p>
                                <ul>
                                    <li>Понедельник - 18:00 </li>
                                    <li>Среда - 14:00 </li>
                                    <li>Суббота - 18:00 </li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="accordion_item faq_accordion_item">
                        <div class="accordion_button">
                          <a href="#">
                            <span>Есть ли гарантия того что изучу английский язык за 3 месяца?</span>
                            <div class="arrow_bottom">
                                <img src="img/arrow_down.svg">
                            </div>
                          </a>

                        </div>
                        <div class="faq_item_content item_content">
                            <div class="item_content_block">
                                <p>Да, с помощью нашей уникальной методики, у вас есть возможность быстро и эффективно выучить английский язык в Ташкенте всего лишь за 3 месяца.</p>
                            
                            </div>
                        </div>
                    </div> 
                    <div class="accordion_item faq_accordion_item">
                        <div class="accordion_button">
                          <a href="#">
                            <span>Где вы находитесь?</span>
                            <div class="arrow_bottom">
                                <img src="img/arrow_down.svg">
                            </div>
                          </a>

                        </div>
                        <div class="faq_item_content item_content">
                            <div class="item_content_block">
                                <p>Мы находимся около метро Беруний, здание Узкоргазмасавдо, на втором этаже. Также у нас скоро появятся новые филиалы!</p>
                                
                            </div>
                        </div>
                    </div>  
                    <div class="accordion_item faq_accordion_item">
                        <div class="accordion_button">
                          <a href="#">
                            <span>Можно ли узнать цены на курсы?</span>
                            <div class="arrow_bottom">
                                <img src="img/arrow_down.svg">
                            </div>
                          </a>

                        </div>
                        <div class="faq_item_content item_content">
                            <div class="item_content_block">
                                <p>Да, вы можете нам написать в социальных сетях, или же позвонить и узнать цены на курсы!</p>
                                
                            </div>
                        </div>
                    </div>   
                </div>'



];
