<?php

return [
    'rollin_name' => 'Sizning ismingiz',
    'rollin_surname' => 'Sizning familiyangiz',
    'rollin_number' => 'Sizning telefon raqamingiz',
    'rollin_call' => 'Telefon qilish',
    'rollin_telegram' => 'Telegramga yozish',
    'type_callback' => "Sizga qaysi usulda aloqaga chiqaylik?",
    'rollin_region' => 'Sizning hududingiz',
    'rollin_time' => 'Siz uchun qulay vaqt',
    'slider_meta' => 'Bizning asosiy kurslarimiz',
    'courses_link' => '<div class="courses_link">
                <h4>Bizning qolgan kurslarimiz:</h4>
                <a href="http://city.uz/uz/course/english-in-three-months">Ingliz tili 3 oyda</a>
                <a href="http://city.uz/uz/course/ingliz-tili-olti-oyda">Ingliz tili 6 oyda</a>
                <a href="http://city.uz/uz/course/ielts-in-month">IELTS 30 kunda</a>
                <a href="http://city.uz/uz/course/ielts">IELTS > 7.0 3 oyda</a>
                <a href="http://city.uz/uz/course/city-speaking">Speaking 3 oyda</a>
                <a href="http://city.uz/uz/course/speak">Ingliz tilida 30 kunda gaplashing</a>
                <a href="http://city.uz/uz/course/sunday-kids">Ingliz tili kurslari bolalar uchun</a>
            </div>
    </div>',
	'courses_duration' => 'O`qish muddati',
    'courses_week' => 'Darslar soni(haftasiga)',
    'courses_time' => 'Darslar soati',
    'about' => 'Biz haqida',
    'courses' => 'Kurslar',
    'open' => "Ochish",
    'region' => "Toshkent",
    'region2' => "Toshkent viloyati",
    'region3' => "Andijon viloyati",
    'region4' => "Buxoro viloyati",
    'region5' => "Jizzax viloyati",
    'region6' => "Qashqadaryo viloyati",
    'region7' => "Navoiy viloyati",
    'region8' => "Namangan viloyati",
    'region9' => "Samarqand viloyati",
    'region10' => "Surxondaryo viloyati",
    'region11' => "Sirdaryo viloyati",
    'region12' => "Farg‘ona viloyati",
    'region13' => "Xorazm viloyati",
    'region14' => " Qoraqalpog‘iston Respublikasi",
    'courses_english3' => 'Ingliz tili 3 oyda',
    'courses_english6' => 'Ingliz tili 6 oyda',
    'courses_corporate' => "Korporativ o'qish",
    'statistycs' => 'Statistika',
    'gallery' => 'Galereya',
    'testimonials' => 'Sharhlar',
    'news' => 'Xabarlar',
    'filial' => 'Filiallar',
    'contacts' => 'Kontaktlar',
    'main' => 'Bosh sahifa',
    'rollin' => "Ro'yhatdan o'tish",
    'rollin_title' => "Bepul sinov darsga ro'yhatdan o'ting!",
    'back' => "Bosh sahifaga qaytish",
    'partners_title' => 'Bizning hamkorlarimiz',
    'address' => 'Adres',
    'orientir' => "Mo'ljal",
    'courses_subtitle' => "Ingliz tilini<br>tez vaqtda o'rganmoqchimisiz?",
    'courses_title' => "Bizning o'quv kurslarimiz",
    'signup' => "Kursga ro'yxatdan o'tish",
    'courses_title' => 'Bizning kurslarimiz',
    'callback' => 'Qayta aloqa',
    'contacts_subtitle' => 'Bizning kontaklarimiz',
    'slider_title' => 'Ingliz tili <br>kurslari',
    'slider_title2' => 'Ingliz tili o`quv markazi!',
    'slider_title3' => 'Ingliz tili 3 oyda',
    'slider_title4' => '30 kunda Ingliz tilida gaplashing!',
    'slider_title5' => 'IELTS 30 kunda',
    'slider1_subtitle1' => 'Sinov mashg`uloti uchun ro`yxatdan o`tmoqchimisiz?',
    'slider1_subtitle2' => 'Savollariz bormi?',
    'slider2_subtitle' => "City Education - Toshkentdagi ingliz tili o'quv markazlari ichida eng kuchli va samarali tarzda tilni o'rgatadi.",
    'slider3_subtitle' => "Ushbu loyihaga ko'ra gapirish (speaking), grammatika (grammar) va o'qib tushunish (reading) darslari asosiy darslar hisoblanib, ularning har birini alohida-alohida o'qituvchi bir soatdan olib boradi va o'quvchilar o'tilgan mavzularni mustahkam egallashlari uchun ular bilan yana bitta o'qituvchi bir soatdan qo'shimcha shug'ullanadi.",
    'slider4_subtitle' => "Ingliz tilida qisqa muddatda ravon gapira olishni xohlaysizmi? Siz va sizga o’xshab Ingliz tili kurslariga qatnab, lekin gapira olmayotganlar uchun maxsus taklif! ",
    'slider5_subtitle' => "Qisqa muddat ichida IELTS imtihonidan baland bal olishni istaysizmi? Oliygohlarga kirishga uchun kerakli ballni qo’lga kiritish uchun vaqt ozmi?",
    'result_text1' => '<span>2013</span> yildan',
    'result_text2' => '<span>8000</span> tadan ortiq o`quvchilar',
    'result_text3' => '<span>2019</span> yilda!',
    'slider1_btn1' => "Ro'yhatdan o'tish",
    'slider1_btn2' => 'Savol berish',
    'slider2_btn1' => 'Tijorat taklifini yuklash',
    'slider2_btn2' => "Ro'yhatdan o'tish",
    'slider4_btn' => "<a href='http://city.uz/uz/course/speak'>Batafsil</a>",
    'slider5_btn' => "<a href='http://city.uz/uz/course/ielts-in-month'>Batafsil</a>",
    'teachers_title' => "Bizning ustozlarimiz",
    'teachers_subtitle' => "Biz bilan hatto kartoshka ham ingliz tilida gaplashishni boshlidi!",
    'features_title' => "Biz boshqalardan juda farq qilamiz",
    'features_subtitle' => "                                 Barcha eng zo’r bo’lishni istaydi, biz esa allaqachon eng zo’rlardanmiz!",
    'features_box_title' => "Biz kafolat beramiz",
    'features_box_title2' => "Bizning va'dalarimiz",
    'features_box_title3' => "Nima uchun bizni tanlashiz kerak?",
    'features_box_title4' => "Bizning afzalliklarimiz",
    'features_box_text' => "To'laydigan pulingizga kafolat xatini olasiz!",
    'features_box_text2' => "Ingliz tilini o'rgana olmasangiz pulingizni qaytarib beramiz!",
    'features_box_text3' => "O'qitish va taraqqiyotning sifatini doimiy nazorat qilish!",
    'features2_box_text' => 'Noyob va zamonaviy tizim!',
    'features2_box_text2' => 'Do’stona va iliq munosabatlar!',
    'features2_box_text3' => 'Qulay va xush yoquvchi muhit!',
    'features3_box_text' => "Biz eng samarali ingliz tili o'quv kurslarini ta'minlaymiz!",
    'features3_box_text2' => "Eng zor ta'lim xizmatini yaratamiz!",
    'features3_box_text3' => "Har doim va hamma joyda birinchi o'rinda!",
    'features4_box_text' => "Har bir o'quvchilarimiz bilan mustahkam aloqada!",
    'features4_box_text2' => 'Biz yuzaga kelgan savollarni qisqa vaqt ichida hal qilamiz!',
    'features4_box_text3' => "Biz hech qachon muvaffaqiyatsizlikka uchratmaymiz va so'zimizni saqlaymiz!",
    'statistycs_subtitle' => "City Education o'quv markazi raqamlarda",
    'statistycs_item' => "talabalar / o'quvchilar biz bilan ingliz tilini o'rganadilar",
    'statistycs_item2' => 'IELTS yoki CEFR sertifikatini olgan',
    'statistycs_item3' => "dan ortiq tajribali o'qituvchilar",
    'statistycs_item4' => "Toshkent bo'ylab filiallar",
    'gallery_title' => 'Galereya',
    'gallery_subtitle' => "Bizning o'quv markazimiz",
    'news_title' => 'Ingliz tili darslari',
    'news_subtitle' => 'Biz bilan ingliz tilini o`rganish yanada osonroq!',
    'testimonials_title' => 'Sharhlar',
    'testimonials_subtitle' => 'Talabalarimizning fikrlari',
    'filial_title' => 'Filiallar',
    'filial_subtitle' => "Bizning kontakt ma'lumotlarimiz",
    'testimonials_btn' => 'Fikr-mulohaza qoldiring',
    'more_news_title' => 'Sizni qiziqtirishi mumkin',
    'more' => "Yangiliklar sahifasiga o'tish",
    'form_number' => 'Sizning telefon raqamingiz',
    'form_mail' => 'Sizning E-mailingiz',
    'form_msg' => 'Sizning xabaringiz',
    'form_title' => "So'rovni qoldiring",
    'form_title2' => 'Bepul dars',
    'form_name' => 'Sizning ismingiz',
    'form_title3' => "Korporativ ta'lim",
    'form_title4' => 'Fikr-mulohaza qoldirish',
    'send_btn' => 'Yuborish',
    'form_date' => 'Sana',
    'form_status' => 'Sizning maqomingiz',
    'student' => 'Talaba',
    'teacher' => 'Ustoz',
    'parent' => 'Ota-ona',
    'company_name' => 'Kompaniyangizning nomi',
    'pupils_amount' => "O'quvchilar soni",
    'lessons_amount' => 'Haftada darslar soni',
    'readmore' => "Batafsil",
    'faq_title' => "Ko'p beriladigan savollar",
    'faq_list' => '<div class="accordion_list">          
                    <div class="accordion_item faq_accordion_item">
                        <div class="accordion_button">
                          <a href="#">
                            <span>Qachon bepul sinov darsiga kelish mumkin?</span>
                            <div class="arrow_bottom">
                                <img src="img/arrow_down.svg">
                            </div>
                          </a>

                        </div>
                        <div class="faq_item_content item_content">
                            <div class="item_content_block">
                                <p>Sinov darslar kuni:</p>
                                <ul>
                                    <li>Dushanba - 10:00 </li>
                                    <li>Chorshanba - 18:00 </li>
                                    <li>Shanba - 14:00 </li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="accordion_item faq_accordion_item">
                        <div class="accordion_button">
                          <a href="#">
                            <span>3 oyda ingliz tilini 3 oyda ingliz tilini Upper-Intermediate darajasigacha o’rganish kafolati bormi?</span>
                            <div class="arrow_bottom">
                                <img src="img/arrow_down.svg">
                            </div>
                          </a>

                        </div>
                        <div class="faq_item_content item_content">
                            <div class="item_content_block">
                                <p>Ha, bizning metodikamiz bilan sizda ingliz tilini 3 oyda o`rganishizning imkoniyati bor</p>
                            
                            </div>
                        </div>
                    </div> 
                    <div class="accordion_item faq_accordion_item">
                        <div class="accordion_button">
                          <a href="#">
                            <span>Sizning markazingiz qayerda joylashgan?</span>
                            <div class="arrow_bottom">
                                <img src="img/arrow_down.svg">
                            </div>
                          </a>

                        </div>
                        <div class="faq_item_content item_content">
                            <div class="item_content_block">
                                <p>Biz Beruniy metroni oldida joylashganmiz, Uzko`rgazmasavdo markazini 2-qavatida</p>
                                
                            </div>
                        </div>
                    </div>  
                    <div class="accordion_item faq_accordion_item">
                        <div class="accordion_button">
                          <a href="#">
                            <span>Kurslar bo’yicha narxlarni qayerdan bilish mumkin?</span>
                            <div class="arrow_bottom">
                                <img src="img/arrow_down.svg">
                            </div>
                          </a>

                        </div>
                        <div class="faq_item_content item_content">
                            <div class="item_content_block">
                                <p>Bizga telefon qilib kurslarni narxini bilib olishingiz mumkin, yoki instagram yoki facebookdan bizdan so`rashingiz mumkin</p>
                                
                            </div>
                        </div>
                    </div>   
                </div>'

];
