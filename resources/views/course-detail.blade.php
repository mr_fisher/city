@extends('layouts.app')

@section('title', $courseItem->getTranslatedAttribute('title', $locale, 'ru'))
@section('description', "Курсы IELTS в Ташкенте, CEFR в Ташкенте, разговорный английский, английский язык для детей в Ташкенте, Ingliz tili o'quv markazi")

@section('content')
<section class="breadcumbs_section courses_breadcumbs">
    <div class="container">
        <div class="row">
            <div class="breadcumb_text">
                <h1>{{$courseItem->getTranslatedAttribute('title', $locale, 'ru')}}</h1>
                <div class="links">
                    <a href="{{route('home', ['locale' => $locale])}}">{{__('message.main')}} <span>/</span></a>
                    <span>{{$courseItem->getTranslatedAttribute('title', $locale, 'ru')}}</span>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="course_section">
	<div class="container">
		<div class="row">
			<div class="course_img">
			<img src="{{asset(($courseItem->image)?'storage/'. $courseItem->image:'img/no-photo.png')}}">
			</div>
			<div class="course_desc">
				<h2 class="course_title">{{$courseItem->getTranslatedAttribute('title', $locale, 'ru')}}</h2>
				<div class="list">
					<div class="list_item">{{__('message.courses_duration')}} : {{$courseItem->getTranslatedAttribute('duration_of_course', $locale, 'ru')}}</div>
					<div class="list_item">{{__('message.courses_week')}} : {{$courseItem->getTranslatedAttribute('number_of_lessons', $locale, 'ru')}}</div>
					<div class="list_item">{{__('message.courses_time')}} : {{$courseItem->getTranslatedAttribute('duration_of_lesson', $locale, 'ru')}}</div>
					<!-- <div class="list_item">График: выбираете сами(с 09:00 до 21:00)</div> -->
				</div>
				<div class="btns">
					<a href="#form" >{{__('message.rollin')}}</a>
				</div>	
			</div>
			<div class="course_text">
				{!!$courseItem->getTranslatedAttribute('desc', $locale, 'ru')!!}
				<div class="btns">
					<a href="#">{{__('message.rollin')}}</a>
				</div>	
			</div>
			{!!__('message.courses_link')!!}
			<div class="link">
				<a href="{{route('home', ['locale' => $locale])}}">&#8592; {{__('message.back')}}</a>
			</div>
			
		</div>
	</div>
</section>

@endsection

@section('scripts')
<script>
	
</script>
@stop