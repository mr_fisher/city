@extends('layouts.app')

@section('title', 'Галерея')

@section('content')
	<section class="breadcumbs_section">
	    <div class="container">
	        <div class="row">
	            <div class="breadcumb_text">
	                <h1>{{__('message.gallery_title')}}</h1>
	                <div class="links">
	                    <a href="{{route('home', ['locale' => $locale])}}">Главная<span>/</span></a>
	                    <span>{{__('message.gallery_title')}}</span>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
	<section class="filter_btns">
		<div class="container">
			<div class="row">
			@foreach($galery->subCategory as $key => $item)
				@if(!empty($item->gallery) && count($item->gallery) > 0)
					<a href="#" data-gallery-open="{{$item->id}}">{{$item->getTranslatedAttribute('title', $locale, 'ru')}}</a>
				@endif
			@endforeach
			</div>
		</div>
	</section>

	@foreach($galery->subCategory as $key => $item)
	@if(!empty($item->gallery) && count($item->gallery) > 0)
		<section class="gallery_item_section">
			<div class="container">
				<div class="gallery_row" data-gallery="{{$item->id}}">
					<div class="row">
					@foreach($item->gallery as $k => $gal)
						<div class="col-lg-4 col-md-6 col-sm-6 col-12">
							<div class="gallery_item_img">
								<img src="{{asset(($gal->image)?'storage/'.$gal->image:'img/no-photo.png')}}">
								<div class="zoom">
									<img src="{{asset('img/loupe.png')}}">
								</div>
							</div>
						</div>
					@endforeach
					</div>
				</div>
				
			</div>
		</section>
		<div class="modal img_modal">
			<div class="modal_content">
				<img src="{{asset('img/banner2.jpeg')}}">
			</div>
			<div class="close_modal">
				<img src="{{asset('img/cancel.svg')}}">
			</div>
		</div>
	@endif
	@endforeach
	<div class="container">
		<div class="link gallery-link">
			<a href="{{route('home', ['locale' => $locale])}}">&#8592; {{__('message.back')}}</a>
		</div>
	</div>
@stop
