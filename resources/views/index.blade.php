@extends('layouts.app')

@if ( Config::get('app.locale') == 'ru') 
	@section('title', 'Английский язык в Ташкенте, курсы английского языка в Ташкенте')
	@section('description', 'Освойте английский язык в Ташкенте за 3 месяца! Запишитесь на курсы английского языка. Учебный центр английского языка в Ташкенте. IELTS')
@elseif( Config::get('app.locale') == 'uz' ) 
	@section('title', "Ingliz tili o'quv markazi Toshkentda. Ingliz tili kurslari Toshkentda. ")
	@section('description', "Ingliz tili intensiv kurslari. Ingliz tilida 30 kunda gaplashing. IELTS kurslari. Ingliz tili bolalar uchun. ")
@endif


@section('keywords', 'Курсы английского языка в Ташкенте')

@section('content')

@include('partials._slider')

@if($courses)

@include('partials._course', ['locale' => $locale, 'courses' => $courses])

@endif


@include('partials._features')

@include('partials._statistic', ['locale' => $locale, 'statistika' => $statistika])
@include('partials._news', ['locale' => $locale, 'news' => $news])

@include('partials._review', ['locale' => $locale, 'reviews' => $reviews])

@include('partials._about', ['locale' => $locale, 'about' => $about, 'partners' => $partners])

@include('partials._branch', ['locale' => $locale, 'branches' => $branches])
<div class="modal sign_in_modal main_lesson_modal">
  <div class="close_modal">
    <img src="{{asset('img/cancel.svg')}}">
  </div>
  <div class="modal_content modal_contest">
    <div class="contest_block">
	      
	      <div class="contest_form">
	        <form id="slider_contest_form" method="POST">
				@csrf
				<input type="hidden" name="type" value="1">
	          <h4>{{__('message.rollin_title')}}</h4>
	        
	          <div class="input_block">
	            <p>Ism / имя</p>
	            <input type="text" name="first_name" required>
	          </div>
	          <div class="input_block">
	            <p>Familiya / фамилия</p>
	            <input type="text" name="last_name" required>
	          </div>
	          <div class="input_block">
	            <p>Telefon raqamingiz / номер телефона</p>
	            <input type="text" name="phone" value="998" pattern=".{14,14}" required>
	          </div>
	          
	          <div class="input_block">
	            <p>Выберите ваш курс</p>
	            <div class="radio">
	              <input type="radio" value="Разговорный английский
	               за месяц" name="course" id="17" required checked>
	              <label for="17">Разговорный английский за 30 дней</label>
	            </div>
	            <div class="radio">
	              <input type="radio" value="IELTS за 30 дней" name="course" id="18">
	              <label for="18">IELTS за 30 дней</label>
	            </div>
	            <div class="radio">
	              <input type="radio" value="Английский за 3 месяца" name="course" id="15" checked >
	              <label for="15">Английский за 3 месяца</label>
	            </div>
	            <div class="radio">
	              <input type="radio" value="Английский за 6 месяцев" name="course" id="16">
	              <label for="16">Английский за 6 месяцев</label>
	            </div>
	            
	          </div>
	          
	          <div class="input_block">
	            <p>Siz uchun qulay vaqt / Подходящее для вас время</p>
	            
	            <div class="radio">
	              <input type="radio" value="09:00" name="time" id="1" required checked>
	              <label for="1">09:00</label>
	            </div>
	            <div class="radio">
	              <input type="radio" value="12:00" name="time" id="2">
	              <label for="2">12:00</label>
	            </div>
	            <div class="radio">
	              <input type="radio" value="15:00" name="time" id="3">
	              <label for="3">15:00</label>
	            </div>
	            <div class="radio">
	              <input type="radio" value="18:00" name="time" id="4">
	              <label for="4">18:00</label>
	            </div>
	          </div>
	          <div class="input_block submit_block">
	            <input type="submit" value="Отправить" name="submit">
	          </div>
	        </form>
	      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
// $('#courses .service_item .btn_block .apply').on('click', function(e){
//     e.preventDefault()

//     var course = $(this).parents('.item_content').find('h4').text()

//     $('#contacts_form .main_course').val(course);
//     $('#contacts_form h4').text(course);

//     alert()

// })
</script>
@endsection