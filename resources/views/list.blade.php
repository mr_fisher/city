@extends('layouts.app')

@section('description', 'City Education приглашает вас на курсы английского языка в Ташкенте. Выучите английский язык всего лишь за 3 месяца вместе с нами!')
@section('keywords', 'Английский язык за 3 месяца')

@section('title', 'Изучайте английский онлайн вместе с нами!')

@section('content')


<section class="breadcumbs_section">
    <div class="container">
        <div class="row">
            <div class="breadcumb_text">
                <h1>Список на пробный урок</h1>
            </div>
        </div>
    </div>
</section>

<?php
    $connection = mysqli_connect('127.0.0.1', 'cityuzdbuser2018', 'RIP_Chester1104', 'city_uz_pupil_data');
    $connection->set_charset("utf8");
    if($connection == false){
        echo 'Не удалось подключиться';
        echo mysqli_connect_error();
        exit();
    }
    $result = mysqli_query($connection, "SELECT * FROM `pupil_data`");

?>
<section class="list_section">
    <div class="container">
        <div class="table">
        <table border="1">
            <?php
                $i = 0;
                while($rows = mysqli_fetch_assoc($result)){
                    $i++;
                
            ?>
            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $rows['name']; ?></td>
                <td><?php echo $rows['surname']; ?></td>
                <td><?php echo $rows['phone']; ?></td>
                <td><?php echo $rows['courses']; ?></td>
                <td><?php echo $rows['time']; ?></td>
                <td><?php echo $rows['type']; ?></td>
                <td><?php echo $rows['form_time']; ?></td>
            </tr>
            <?php
                }
            ?>
        </table> 
        </div>
    </div>
</section>

@endsection