@extends('layouts.app')

@section('title', $newsItem->getTranslatedAttribute('title', $locale, 'ru'))

@section('content')

<section class="breadcumbs_section">
    <div class="container">
        <div class="row">
            <div class="breadcumb_text">
                <h1>{{__('message.news_title')}}</h1>
                <div class="links">
                    <a href="{{route('home', ['locale' => $locale])}}">{{__('message.main')}} <span>/</span></a>
                    <a href="{{route('news', ['locale' => $locale])}}"> {{__('message.news_title')}}<span>/</span></a>
                    <span>{{substr($newsItem->getTranslatedAttribute('title', $locale, 'ru'), 0, 30)}}...</span>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="post_section">
    <div class="container">
        <div class="row">
            <div class="post_block col-md-8">
                <h1>{{$newsItem->getTranslatedAttribute('title', $locale, 'ru')}}</h1>
                <h4>{{date('d.m.Y', strtotime($newsItem->created_at))}}</h4>
                <img src="{{asset(($newsItem->image)?'storage/'. $newsItem->image:'img/no-photo.png')}}">
                <div class="short_desc">
                    {!!$newsItem->getTranslatedAttribute('short_desc', $locale, 'ru')!!}
                </div>
				{!!$newsItem->getTranslatedAttribute('desc', $locale, 'ru')!!}
                <div class="rambler-share" style="margin-top: 30px;"></div>
                <div class="btn_block">
                    <a href="{{route('news', ['locale' => $locale])}}">{{__('message.more')}}</a>
                </div>
            </div>
            <div class="news_list col-md-4">
				@if(!empty($news))

                @foreach($news as $key => $item)

				<div class="news_list_block">
                    <h4>{{__('message.more_news_title')}}</h4>
                    <div class="news_block">
                        <a href="{{route('newsDetail', ['url' => $item->url, 'locale' => $locale])}}" class="news_item">
                            <img data-src="{{asset(($item->image)?'storage/'. $item->image:'img/no-photo.png')}}" class="lozad">
                            <div class="news_desc">
                                <h1>{{substr($item->getTranslatedAttribute('title', $locale, 'ru'), 0, 30)}}...</h1>
                                <h5>{{date('d.m.Y', strtotime($item->date))}}</h5>
                                <div class="text">
                                    {!!$item->getTranslatedAttribute('short_desc', $locale, 'ru')!!}...
                                </div>

                            </div>
                        </a>
                    </div>
                </div>

				@endforeach

				@endif

            </div>
        </div>
    </div>
</section>
<script>
(function() {
var init = function() {
RamblerShare.init('.rambler-share', {
    "style": {
        "buttonHeight": 34,
        "iconSize": 20,
        "borderRadius": 50,
        "counterSize": 12
    },
    "utm": "utm_medium=social",
    "counters": true,
    "buttons": [
        "vkontakte",
        "facebook",
        "odnoklassniki",
        "telegram",
        "whatsapp",
        "messenger",
        "copy"
    ]
});
};
var script = document.createElement('script');
script.onload = init;
script.async = true;
script.src = 'https://developers.rambler.ru/likes/v1/widget.js';
document.head.appendChild(script);
})();
</script>
<!------   Rambler.Likes script end  ------>
@endsection