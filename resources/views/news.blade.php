@extends('layouts.app')

@section('description', 'City Education приглашает вас на курсы английского языка в Ташкенте. Выучите английский язык всего лишь за 3 месяца вместе с нами!')
@section('keywords', 'Английский язык за 3 месяца')

@section('title', 'Изучайте английский онлайн вместе с нами!')

@section('content')

<section class="breadcumbs_section">
    <div class="container">
        <div class="row">
            <div class="breadcumb_text">
                <h1>{{__('message.news_title')}}</h1>
                <div class="links">
                    <a href="{{route('home', ['locale' => $locale])}}">{{__('message.main')}} <span>/</span></a>
                    <span> {{__('message.news_title')}}</span>
                </div>
            </div>
        </div>
    </div>
</section>


@if(!empty($news))
<section class="news_section">
    <div class="container">
        <div class="section_title">
            <h2>{{__('message.news_title')}}</h2>
            <p>{{__('message.news_subtitle')}}</p>
        </div>
        <div class="row">

        	@foreach($news as $key => $item)

            @include('partials._news_item', ['locale' => $locale, 'item' => $item])

            @endforeach

        </div>
	    {{$news->links()}}
    </div>
</section>
@endif

@endsection