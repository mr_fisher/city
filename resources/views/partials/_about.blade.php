<section class="about_section" id="about">
	<div class="container">
		<div class="section_title">
			<h2>{{$about->getTranslatedAttribute('title', $locale, 'uz')}}</h2>
			<p>{{$about->getTranslatedAttribute('short_desc', $locale, 'uz')}}</p>
		</div>
		<div class="row">
			<div class="col-lg-12 about_text">
				<div class="about_text_block">
					{!!$about->getTranslatedAttribute('desc', $locale, 'uz')!!}
				</div>
			</div>
			@if($partners)
			<div class="col-lg-12 partners_logo">
				<h4>{{__('message.partners_title')}}</h4>
				<div class="partners_row row">
					@foreach($partners as $key => $partner)
					<div class="partners_logo_img">
						<a href="{{$partner->url}}" target="_blank">
							<img src="{{asset(($partner->image)?'storage/'.$partner->image:'img/no-photo.png')}}">
						</a>
					</div>
					@endforeach
				</div>
			</div>	
			@endif
			
		</div>
	</div>
</section>