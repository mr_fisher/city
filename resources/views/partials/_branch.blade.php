<section class="contacts_section" id="contacts">
	<div class="section_title">
		<h2>{{__('message.filial_title')}}</h2>
		<p>{{__('message.filial_subtitle')}}</p>
	</div>
	<div class="container">
		<div class="row">
			@foreach($branches as $key => $branch)
			<div class="col-md-6">
				{!!$branch->map!!}
				<div class="contact_desc">
					<h6><span>{{$branch->getTranslatedAttribute('title', $locale, 'uz')}}</span></h6>
					<h6><span>{{__('message.address')}}:</span> {{$branch->getTranslatedAttribute('address', $locale, 'uz')}}</h6>
					<h6><span>{{__('message.orientir')}}:</span> {{$branch->getTranslatedAttribute('orientir', $locale, 'uz')}}</h6>
				</div>
			</div>
			@endforeach
		</div>	
	</div>
</section>
