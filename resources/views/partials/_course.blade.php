<section class="courses_section" id="courses">
	<div class="section_title">
		<h2>{{__('message.courses_title')}}</h2>
		<p>{!!__('message.courses_subtitle')!!}</p>
	</div>
	<div class="row">
		@php $i = 1 @endphp
		@foreach($courses as $key => $course)

		@if($i == 1)
		<div class="col-lg-9 col-md-12">
		@endif
			<div class="service_item">
				<div class="item_content">
					<h4><span>{{$course->getTranslatedAttribute('title', $locale, 'uz')}}</span>
						<img src="img/arrow_down.svg"></h4>
					<div class="text_content">
						{!! $course->getTranslatedAttribute('desc', $locale, 'uz') !!}
					</div>
					<div class="courses_row">
						<ul>
							<li>{{__('message.courses_duration')}} : {{$course->getTranslatedAttribute('duration_of_course', $locale, 'uz')}}</li>
							<li>{{__('message.courses_week')}} : {{$course->getTranslatedAttribute('number_of_lessons', $locale, 'uz')}}</li>
							<li>{{__('message.courses_time')}} : {{$course->getTranslatedAttribute('duration_of_lesson', $locale, 'uz')}}</li>
						</ul>
						<div class="btn_block">
							<a href="javascript:void(0);" class="apply">{{__('message.signup')}}</a>
							<a href="{{route('courseDetail', ['locale' => $locale, 'url' => $course->url])}}">{{__('message.readmore')}}</a>
						</div>
					</div>
				</div>
			</div>
		@if($i == 2)
			<?php $i = 1 ?>
			</div>
		@else
			<?php $i++ ?> 
		@endif
		@endforeach
	</div>
</section>