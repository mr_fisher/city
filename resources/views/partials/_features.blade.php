<section class="features_section" id="features">
	<div class="container">
		<div class="section_title">
			<h2>{{__('message.features_title')}}</h2>
			<p>{{__('message.features_subtitle')}}</p>
		</div>
		<div class="features_slider_block">
			<div class="features_slider">
				<div class="slider_item row features_list side1">
					<div class="slider_item_block">
						<h3>{{__('message.features_box_title')}}</h3>
						<div class="list">
							<div class="list_item">
								<div class="list_num">
									<h2>01</h2>
								</div>
								<div class="list_desc">
									<h4>{{__('message.features_box_text')}}</h4>
								</div>
							</div>
							<div class="list_item">
								<div class="list_num">
									<h2>02</h2>
								</div>
								<div class="list_desc">
									<h4>{{__('message.features_box_text2')}}</h4>
								</div>
							</div>
							
							<div class="list_item">
								<div class="list_num">
									<h2>03</h2>
								</div>
								<div class="list_desc">
									<h4>{{__('message.features_box_text3')}}</h4>
								</div>
							</div>
						</div>
					</div>
					
				</div>
				<div class="slider_item row features_list side2">
					
					<div class="slider_item_block">
						<h3>{{__('message.features_box_title2')}}</h3>
						<div class="list">
							<div class="list_item">
								<div class="list_num">
									<h2>01</h2>
								</div>
								<div class="list_desc">
									<h4>{{__('message.features2_box_text')}}</h4>
								</div>
							</div>
							<div class="list_item">
								<div class="list_num">
									<h2>02</h2>
								</div>
								<div class="list_desc">
									<h4>{{__('message.features2_box_text2')}}</h4>
								</div>
							</div>
							<div class="list_item">
								<div class="list_num">
									<h2>03</h2>
								</div>
								<div class="list_desc">
									<h4>{{__('message.features2_box_text3')}}</h4>
								</div>
							</div>
						</div>
					</div>
					
				</div>
				<div class="slider_item row side3">
					<div class="list_title">
						<h3>{{__('message.features_box_title3')}}</h3>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-6">
						<img src="img/calendar.svg">
						<h3>{{__('message.features3_box_text')}}</h3>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-6">
						<img src="img/goal.svg">
						<h3>{{__('message.features3_box_text2')}}</h3>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-6">
						<img src="img/startup.svg">
						<h3>{{__('message.features3_box_text3')}}</h3>
					</div>
				</div>
				<div class="slider_item features_list side4">
					<div class="slider_item_block">
						<h3>{{__('message.features_box_title4')}}</h3>
						<div class="list">
							
							<div class="list_item">
								<div class="list_num">
									<h2>01</h2>
								</div>
								<div class="list_desc">
									<h4>{{__('message.features4_box_text')}}</h4>
								</div>
							</div>
							<div class="list_item">
								<div class="list_num">
									<h2>02</h2>
								</div>
								<div class="list_desc">
									<h4>{{__('message.features4_box_text2')}}</h4>
								</div>
							</div>
							<div class="list_item">
								<div class="list_num">
									<h2>03</h2>
								</div>
								<div class="list_desc">
									<h4>{{__('message.features4_box_text3')}}</h4>
								</div>
							</div>
						</div>
					</div>

				</div>
				
			</div>
			<button class="next"><img src="img/arrow_down.svg"></button>
			<button class="prev"><img src="img/arrow_down.svg"></button>
		</div>
		
		
		
	</div>
</section>