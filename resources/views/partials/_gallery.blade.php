<section class="gallery_section" id="gallery">
	<div class="section_title">
		<h2>{{__('message.gallery_title')}}</h2>
		<p>{{__('message.gallery_subtitle')}}</p>
	</div>
	<div class="row no-gutters">
		@foreach($galery as $key => $item)
		
		@if(!empty($item->subCategory) && count($item->subCategory) > 0)
		{{--<div class="col-sm-6 gallery_category" data-gallery-open="gallery_{{$item->id}}">--}}
		<div class="col-sm-6 gallery_category">
			<a href="{{route('gallery', ['locale' => $locale, 'id' => $item->id])}}">
				<h3>{{$item->getTranslatedAttribute('title', $locale, 'uz')}}</h3>
				<img src="{{asset(($item->image)?'storage/'.$item->image:'img/no-photo.png')}}">
			</a>
		</div>
		@endif

		@endforeach
	</div>	
</section>

@foreach($galery as $key => $item)

@if(!empty($item->subCategory) && count($item->subCategory) > 0)

<div class="modal gallery_modal" data-gallery="gallery_{{$item->id}}">
	<div class="container">
		<div class="row">
			<div class="slider_block">

				@foreach($item->subCategory as $k => $subCat)

				<div class="gallery_carousel active" data-slider="{{$subCat->id}}">

					@foreach($subCat->gallery as $k => $gal)

					<div class="carousel_item">
						<img data-src="{{asset(($gal->image)?'storage/'.$gal->image:'img/no-photo.png')}}" class="lozad">
					</div>

					@endforeach

				</div>

				@endforeach

			</div>

			<div class="gallery_category">

				@foreach($item->subCategory as $k => $subCat)

				<a href="javascript:void(0);" class="gallery_item" data-slider-open="{{$subCat->id}}">
					<h3>{{$subCat->getTranslatedAttribute('title', $locale, 'uz')}}</h3>
				</a>

				@endforeach

			</div>

		</div>
	</div>
	<div class="close_modal">
		<img src="{{asset('img/cancel.svg')}}">
	</div>
</div>

@endif

@endforeach


