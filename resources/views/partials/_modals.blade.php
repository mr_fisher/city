<div class="contact_call">
	<img src="{{asset('img/call2.svg')}}">
</div>
<div class="modal sign_in_modal contacts_modal">
	<div class="modal_content modal_contest container">
		<div class="contest_block">
			<div class="contest_form">
				<form method="POST" id="contacts_form">
					@csrf
					<input type="hidden" name="course" value="">
					<input type="hidden" name="type" value="1">

					  <h4>{{__('message.rollin_title')}}</h4>

					  <div class="input_block">
					    <p>{{__('message.rollin_name')}}</p>
					    <input type="text" name="first_name" required>
					  </div>
					  <div class="input_block">
					    <p>{{__('message.rollin_surname')}}</p>
					    <input type="text" name="last_name" required>
					  </div>
					  <div class="input_block">
					    <p>{{__('message.rollin_number')}}</p>
					    <input type="text" name="phone" value="998" pattern=".{14,14}" required>
					  </div>
					  
					  <div class="input_block">
					    <p>{{__('message.type_callback')}}</p>
					    <div class="radio">
					      <input type="radio" value="Позвонить вам" name="callback_type" id="6" required="" checked>
					      <label for="6">{{__('message.rollin_call')}}</label>
					    </div>
					    <div class="radio">
					      <input type="radio" value="Написать вам в Telegram" name="callback_type" id="7">
					      <label for="7">{{__('message.rollin_telegram')}}</label>
					    </div>
					  </div>

					  <div class="input_block">
					      <p>{{__('message.rollin_region')}}</p>

					    <select name="region">
							<option selected>{{__('message.region')}}</option>
							<option>{{__('message.region2')}}</option>
							<option>{{__('message.region3')}}</option>
							<option>{{__('message.region4')}}</option>
							<option>{{__('message.region5')}}</option>
							<option>{{__('message.region6')}}</option>
							<option>{{__('message.region7')}}</option>
							<option>{{__('message.region8')}}</option>
							<option>{{__('message.region9')}}</option>
							<option>{{__('message.region10')}}</option>
							<option>{{__('message.region11')}}</option>
							<option>{{__('message.region12')}}</option>
							<option>{{__('message.region13')}}</option>
							<option>{{__('message.region14')}}</option>
							
						</select>
					  </div>
					  
					  <div class="input_block time_block">
					    <p>{{__('message.rollin_time')}}</p>
					    
					    <div class="radio ">
					      <input type="radio" value="09:00" name="time" id="1" required>
					      <label for="1">09:00</label>
					    </div>
					    <div class="radio radio_12">
					      <input type="radio" value="12:00" name="time" id="2">
					      <label for="2">12:00</label>
					    </div>
					    <div class="radio">
					      <input type="radio" value="15:00" name="time" id="3">
					      <label for="3">15:00</label>
					    </div>
					    <div class="radio radio_18">
					      <input type="radio" value="18:00" name="time" id="4">
					      <label for="4">18:00</label>
					    </div>
					  </div>
					  <div class="input_block submit_block">
					  	<button type="submit">{{__('message.send_btn')}}</button>
					  </div>
				</form>
			</div>
		</div>
	</div>
	<div class="close_modal">
		<img src="{{asset('img/cancel.svg')}}">
	</div>
</div>
<div class="modal callback_modal">
	<div class="modal_content container">
		<form method="POST" id="callback_form">
			<h4><p>Обратный звонок</p></h4>
			@csrf
			<input type="hidden" name="type" value="2">
			<div class="input_block">
				<p>{{__('message.form_name')}}</p>
				<input type="text" name="first_name" required>
			</div>
			<div class="input_block">
				<p>{{__('message.form_number')}}</p>
				<input type="text" oninvalid="this.setCustomValidity('Пожалуйста, введите девятизначный номер')" name="phone" value="998" maxlength="10" pattern=".{14,14}" required="">
			</div>
			<div class="input_block submit_block">
				<input type="submit" value="{{__('message.send_btn')}}" name="submit">
			</div>
		</form>
	</div>
	<div class="close_modal">
		<img src="{{asset('img/cancel.svg')}}">
	</div>
</div>
<div class="modal zayavka_modal">
	<div class="modal_content container">
		<form method="POST" id="zayavka">
			<h4><p>{{__('message.form_title')}}</p></h4>
			@csrf
			<input type="hidden" name="type" value="3">
			<div class="input_block">
				<p>{{__('message.form_name')}}</p>
				<input type="text" name="first_name" required>
			</div>
			<div class="input_block">
				<p>{{__('message.form_number')}}</p>
				<input type="text" oninvalid="this.setCustomValidity('Пожалуйста, введите девятизначный номер')" name="phone" value="998" maxlength="10" pattern=".{14,14}" required="">
			</div>
			<div class="input_block textarea_block">
				<p>{{__('message.form_msg')}}</p>
				<textarea name="message"></textarea>
			</div>
			<div class="input_block submit_block">
				<input type="submit" value="{{__('message.send_btn')}}" name="submit">
			</div>
		</form>
	</div>
	<div class="close_modal">
		<img src="{{asset('img/cancel.svg')}}">
	</div>
</div>
<div class="modal free_lesson">
	<div class="modal_content container">
		<form method="POST" id="callback_modal">
			<h4>{{__('message.form_title2')}}</h4>
			<div class="input_block">
				<p>{{__('message.form_name')}}</p>
				<input type="text" name="name" required="">
			</div>
			<div class="input_block">
				<p>{{__('message.form_number')}}</p>
				<input type="text" name="phone" oninvalid="this.setCustomValidity('Пожалуйста, введите свой девятизначный номер')" value="998" maxlength="10" pattern=".{14,14}" required="">
			</div>
			<div class="input_block">
				<p>{{__('message.form_msg')}}</p>
				<textarea name="msg"></textarea>
			</div>
			<div class="input_block submit_block">
				<input type="submit" value="{{__('message.send_btn')}}" name="submit">
			</div>
		</form>
	</div>
	<div class="close_modal">
		<img src="{{asset('img/cancel.svg')}}">
	</div>
</div>
<div class="modal corporate_modal">
	<div class="modal_content container">
		<form method="POST" id="callback_modal">
			<h4>{{__('message.form_title3')}}</h4>
			<div class="input_block hide_input">
				<p>Категория заявки</p>
				<select name="category" class="category" disabled="">
					<option selected="">Корпоративное обучение</option>
				</select>
			</div>
			<div class="input_block">
				<p>{{__('message.company_name')}}</p>
				<input type="text" name="name" required="">
			</div>
			<div class="input_block">
				<p>{{__('message.pupils_amount')}}</p>
				<input type="number" name="pupils_amount" required="">
			</div>
			<div class="input_block">
				<p>{{__('message.lessons_amount')}}</p>
				<input type="number" name="lesson_amount" required="">

			</div>
			<div class="input_block">
				<p>{{__('message.form_number')}}</p>
				<input type="text" name="phone" value="+998" oninvalid="this.setCustomValidity('Пожалуйста, введите свой девятизначный номер')" pattern=".{14,14}" required="">
			</div>
			<div class="input_block">
				<p>{{__('message.form_msg')}}</p>
				<textarea name="msg" placeholder="Пример: Добрый день! Нашей компании нужны опытные преподаватели по английскому языку, хотелось бы узнать подробнее о ваших курсах" required=""></textarea>
			</div>
			<div class="input_block submit_block">
				<input type="submit" value="{{__('message.send_btn')}}" name="submit">
			</div>
		</form>
	</div>
	<div class="close_modal">
		<img src="{{asset('img/cancel.svg')}}">
	</div>
</div>
<div class="modal review_modal">
	<div class="modal_content container">
		<form method="POST" id="callback_modal">
			<h4>{{__('message.form_title4')}}</h4>
			<div class="input_block hide_input">
				<input type="text" class="category" value="Отзыв" 
				name="category" disabled="">
			</div>
			<div class="input_block">
				<p>{{__('message.form_name')}}</p>
				<input type="text" name="name" required="">
			</div>
			<div class="input_block">
				<p>{{__('message.date')}}</p>
				<input type="date" class="review_date" name="date" required="">
			</div>
			<div class="input_block">
				<p>{{__('message.status')}}</p>
				<select name="status">
					<option>{{__('message.student')}}</option>
					<option>{{__('message.parent')}}</option>
					<option>{{__('message.teacher')}}</option>
				</select>
			</div>
			<div class="input_block">
				<p>{{__('message.form_msg')}}</p>
				<textarea name="msg" placeholder="" required=""></textarea>
			</div>
			<div class="input_block submit_block">
				<input type="submit" value="{{__('message.send_btn')}}" name="submit">
			</div>
		</form>
	</div>
	<div class="close_modal">
		<img src="{{asset('img/cancel.svg')}}">
	</div>
</div>