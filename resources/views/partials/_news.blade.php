<section class="news_section">
		<div class="container">
			<div class="section_title">
				<h2>{{__('message.news_title')}}</h2>
				<p>{{__('message.news_subtitle')}}</p>
			</div>
			<div class="row">

				@foreach($news as $key => $item)

				@include('partials._news_item', ['locale' => $locale, 'item' => $item])

				@endforeach

			</div>
			<div class="btn_block">
				<a href="{{route('news', ['locale' => $locale])}}">{{__('message.more')}}</a>
			</div>
		</div>
	</section>