<div class="col-lg-3 col-md-6 col-sm-6 col-12">
	<a href="{{route('newsDetail', ['url' => $item->url, 'locale' => $locale])}}" class="news_item">
        <img data-src="{{asset(($item->image)?'storage/'. $item->image:'img/no-photo.png')}}" src="{{asset(($item->image)?'storage/'. $item->image:'img/no-photo.png')}}" class="lozad">
		<div class="news_desc">
			<h1>{{$item->getTranslatedAttribute('title', $locale, 'uz')}}</h1>
			<h5>{{date('d.m.Y', strtotime($item->created_at))}}</h5>
			<div class="text">
				<?php  
				$str = mb_substr($item->getTranslatedAttribute('desc', $locale, 'uz'), 0, 80, 'utf-8');
				?>
				{!! $item->getTranslatedAttribute('short_desc', $locale, 'uz') !!}
			</div>
		</div>
	</a>
</div>