<section class="testimonials_section" id="testimonials">
	<div class="section_title">
		<h2>{{__('message.testimonials_title')}}</h2>
		<p>{{__('message.testimonials_subtitle')}}</p>
	</div>
	<div class="container">
		<button class="flickity-button flickity-prev-next-button previous" type="button" aria-label="Previous"><svg class="flickity-button-icon" viewBox="0 0 100 100"><path d="M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z" class="arrow"></path></svg></button>
		<button class="flickity-button flickity-prev-next-button next" type="button" aria-label="Next"><svg class="flickity-button-icon" viewBox="0 0 100 100"><path d="M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z" class="arrow" transform="translate(100, 100) rotate(180) "></path></svg></button>
		<div class="row testimonials_row">

			@foreach($reviews as $key => $review)
				<?php $i = ++$key ?>
				<input type="radio" name="slider" id="s{{$i}}" {{($key == 2)?'checked="true"':''}}>
			@endforeach
			<!-- <input type="radio" name="slider" id="s2">
			<input type="radio" name="slider" id="s3" checked="true">
			<input type="radio" name="slider" id="s4">
			<input type="radio" name="slider" id="s5"> -->


			@foreach($reviews as $key => $review)
			<?php $i = ++$key ?>
			<label for="s{{$i}}" id="slide{{$i}}">
				<div class="testimonial_item">
					<h4>{{$review->getTranslatedAttribute('name', $locale, 'uz')}}</h4>
					<h5>{{$review->getTranslatedAttribute('activity', $locale, 'uz')}}</h5>
					{!!$review->getTranslatedAttribute('desc', $locale, 'uz')!!}
				</div>
			</label>
			@endforeach
		</div>
	</div>
	<div class="send_review">
		<a href="javascript:void(0);">{{__('message.testimonials_btn')}}</a>
	</div>
</section>