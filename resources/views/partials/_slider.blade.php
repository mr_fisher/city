<section class="slider" id="slider">
	<div class="slider_row">
		<div class="slider_item main_course" style="background: url(img/bg.png);background-size: cover;background-position: center;">
			<div class="container">
				<div class="slider_content">
					<div class="main_course_tag">
						<img src="img/lightning.svg">{!!__('message.slider_meta')!!}
					</div>
					<h1>{!!__('message.slider_title4')!!}</h1>
					<p>{!!__('message.slider4_subtitle')!!}</p>
					<div class="button_block">
						{!!__('message.slider4_btn')!!}
					</div>
				</div>
			</div>
		</div>
		<div class="slider_item main_course" style="background: url(img/section_bg4.jpeg);background-size: cover;background-position: center;">
			<div class="container">
				<div class="slider_content">	
					<h1>{!!__('message.slider_title5')!!}</h1>
					<p>{!!__('message.slider5_subtitle')!!}  </p>
					<div class="button_block ">
						{!!__('message.slider5_btn')!!}
					</div>
				</div>
			</div>
		</div>
		
		
		<div class="slider_item main_course" style="background: url(img/slider_bg6.jpg);background-size: cover;background-position: center;">
			<div class="container">
				<div class="slider_content">
					<div class="main_course_tag">
						<img src="img/lightning.svg">{!!__('message.slider_meta')!!}
					</div>
					<h1>{!!__('message.slider_title3')!!}</h1>
					<p>{!!__('message.slider3_subtitle')!!}</p>
					<div class="button_block ">
						<a href="#/" class="open_form">{{__('message.signup')}}</a>
					</div>
				</div>
			</div>
		</div>
		<div class="slider_item registration" style="background: url(img/about_bg.jpeg);background-size: cover;background-position: bottom;">
			<div class="container">
				<div class="slider_content">
					<h1>{!!__('message.slider_title')!!}</h1>
					
					<div class="button_block slider_block">
						<div class="button_item">
							<p>{!!__('message.slider1_subtitle1')!!}</p>
							<a href="javascript:void(0);" class="lesson">{{__('message.slider1_btn1')}}</a>
						</div>
						
					</div>
				</div>
			</div>
		</div>

		<div class="slider_item" style="background: url(img/slider_bg4.jpg);background-size: cover;background-position: center;">
			<div class="container">
				<div class="slider_content">
					<h1>{!!__('message.slider_title2')!!}</h1>
					<p>{!!__('message.slider2_subtitle')!!}</p>
					<div class="button_block slider_block">
						
						<div class="button_item">
							<p>{!!__('message.slider1_subtitle2')!!}</p>
							<a href="javascript:void(0);" class="question">{{__('message.slider1_btn2')}}</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
	</div>
	<div class="timeline">
		<div class="container">
			<div class="row">
				<div class="line_item">
					<p>{!!__('message.result_text1')!!}</p>
				</div>
				<div class="line_item">
					<p>{!!__('message.result_text2')!!}</p>
				</div>
				<div class="line_item">
					<p>{!!__('message.result_text3')!!}</p>
				</div>
			</div>
		</div>
	</div>
</section>