<section class="count_section" id="count">
	<div class="container">
		<div class="section_title">
			<h2>{{__('message.statistycs')}}</h2>
			<p>{{__('message.statistycs_subtitle')}}</p>
		</div>
		<div class="row">
			<div class="col-md-3 col-sm-6 col-6">
				<img src="img/resume.svg">
				<h3 class='numscroller' data-min='1' data-max='{{$statistika->students}}' data-delay='5' data-increment='10'>{{$statistika->students}}</h3>
				<h4>{{__('message.statistycs_item')}}</h4>
			</div>
			<div class="col-md-3 col-sm-6 col-6">
				<img src="img/certificate.svg">
				<h3 class='numscroller' data-min='1' data-max='{{$statistika->sertificats}}' data-delay='5' data-increment='8'>{{$statistika->sertificats}}</h3>
				<h4>{{__('message.statistycs_item2')}}</h4>
			</div>
			<div class="col-md-3 col-sm-6 col-6">
				<img src="img/education.svg">
				<h3 class='numscroller' data-min='1' data-max='{{$statistika->teachers}}' data-delay='5' data-increment='5'>{{$statistika->teachers}}</h3>
				<h4>{{__('message.statistycs_item3')}}</h4>
			</div>
			<div class="col-md-3 col-sm-6 col-6">
				<img src="img/success.svg">
				<h3 class='numscroller' data-min='1' data-max='{{$statistika->branches}}' data-delay='5' data-increment='1'>{{$statistika->branches}}</h3>
				<h4>{{__('message.statistycs_item4')}}</h4>
			</div>
		</div>
	</div>
</section>
<section class="faq_section">
	<div class="section_title">
		<h2>{!!__('message.faq_title')!!}</h2>
	</div>
	<div class="container">
		<div class="row">
			<div class="accordeon_content">
				{!!__('message.faq_list')!!}

			</div>
		</div>
	</div>
</section>	