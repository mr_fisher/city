<section class="teachers_section">
	<div class="container">
		<div class="section_title">
			<h2>{{__('message.teachers_title')}}</h2>
			<p>{{__('message.teachers_subtitle')}}</p>
		</div>
		<div class="row teachers_row">
			<div class="teachers_row_block">
				@foreach($teachers as $key => $teacher)
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="teacher_item">
						<img src="{{asset(($teacher->image)?'storage/'.$teacher->image:'img/no-photo.png')}}">
						<div class="name">
							<h4>{{$teacher->getTranslatedAttribute('name', $locale, 'uz')}}</h4>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
		<div class="show_more_btn">
			<a href="#">Открыть</a>
		</div>
	</div>
</section>