<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	<meta name="google-site-verification" content="5-CcKJNb3fgp5OzNXIySTQFfMh-y-OjnV3LIT9zkLT0" />
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="{{asset('css/libs.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/media.css')}}">
	<link rel="icon" href="img/favicon.png">

	<link href="https://fonts.googleapis.com/css?family=Rubik:400,700,900&amp;subset=cyrillic" rel="stylesheet">
	<title>@yield('title')</title>
	<meta name="description" content="@yield('description')">
	<meta name="keywords" content="@yield('keywords')">
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131930598-2"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-131930598-2');
	</script>
</head>
<body>
	<?php  
	$route = Route::currentRouteName();
	$about = \App\About::withTranslation($locale)->first();
	?>	
	<script type="text/javascript" >
	   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
	   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
	   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

	   ym(52054707, "init", {
	        id:52054707,
	        clickmap:true,
	        trackLinks:true,
	        accurateTrackBounce:true,
	        webvisor:true
	   });
	</script>
	<noscript><div><img src="https://mc.yandex.ru/watch/52054707" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
	<header>
		<div class="first_header">
			<div class="container">
				<div class="row">
					<div class="contact_block mail_block">
						<a href="{{route('home', ['locale' => $locale])}}" class="icon">
							<img src="{{asset('img/mail.svg')}}">
						</a>
						<div class="contact_text">
							<h4>{{$about->email}}</h4>
						</div>
					</div>

					<div class="lang_block contact_block">
						<a href="{{route('home', ['locale' => 'uz'])}}"><img src="{{asset('img/lang_uz.jpg')}}">UZ</a>
						<a href="{{route('home', ['locale' => 'ru'])}}"><img src="{{asset('img/lang_ru.png')}}">RU</a>
					</div>
					<div class="social_icons">
						<a href="{{$about->facebook}}" target="blank">
							<img src="{{asset('img/fb.svg')}}">
						</a>
						<a href="{{$about->telegram}}" target="blank">
							<img src="{{asset('img/telegram.svg')}}">
						</a>
						<a href="{{$about->instagram}}" target="blank">
							<img src="{{asset('img/instagram.svg')}}">
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="second_header">
			<div class="container">
				<div class="row">
					<div class="header_btn">
						<a href="javascript:void(0);" class="btn">{{__('message.callback')}}</a>
					</div>
					<div class="logo_block">
						<a href="{{route('home', ['locale' => $locale])}}">
							<img src="{{asset('img/city.svg')}}">
						</a>
					</div>

					@if($route == 'home')
					<div class="menu_block">
						<div class="menu">
							<a href="#slider">{{__('message.main')}}</a>
							<a href="#courses">{{__('message.courses')}}</a>
							<!-- <a href="#count">{{__('message.statistycs')}}</a> -->
							<a href="#gallery">{{__('message.gallery')}}</a>
							<a href="{{route('news', ['locale' => $locale])}}">{{__('message.news')}}</a>
							<a href="#testimonials">{{__('message.testimonials')}}</a>
							<a href="#about">{{__('message.about')}}</a>
							<a href="#contacts">{{__('message.contacts')}}</a>
							<a href="javascript:void(0);" class="btn">{{__('message.callback')}}</a>
							<div class="close_icon">
								<img src="{{asset('img/cancel.svg')}}">
							</div>
						</div>
					</div>
					@else
					<div class="menu_block">
						<div class="menu">
							<a href="{{route('home', ['locale' => $locale])}}#slider">{{__('message.main')}}</a>
							<a href="{{route('home', ['locale' => $locale])}}#courses">{{__('message.courses')}}</a>
							<!-- <a href="{{route('home', ['locale' => $locale])}}#count">{{__('message.statistycs')}}</a> -->
							<a href="{{route('home', ['locale' => $locale])}}#gallery">{{__('message.gallery')}}</a>
							<a href="{{route('news', ['locale' => $locale])}}">{{__('message.news')}}</a>
							<a href="{{route('home', ['locale' => $locale])}}#testimonials">{{__('message.testimonials')}}</a>
							<a href="{{route('home', ['locale' => $locale])}}#about">{{__('message.about')}}</a>
							<a href="{{route('home', ['locale' => $locale])}}#contacts">{{__('message.contacts')}}</a>
							<a href="javascript:void(0);" class="btn">{{__('message.callback')}}</a>
							<div class="close_icon">
								<img src="{{asset('img/cancel.svg')}}">
							</div>
						</div>
					</div>
					@endif
					<div class="phone_block">
						<a href="tel:{{$about->phone}}"><img src="{{asset('img/call2.svg')}}">{{$about->phone}}</a>
					</div>	
					<div class="menu_icon">
						<img src="{{asset('img/menu.svg')}}">
					</div>	
				</div>
			</div>
		</div>	
	</header>

	@yield('content', $route)

	<footer>
		<div class="container">
			<div class="row ">
				<div class="col-md-4 col-sm-6 footer_contact">
					<div class="contact_item">
						<img src="{{asset('img/mail.svg')}}">
						<p>{{$about->email}}</p>
					</div>
					<div class="contact_item">
						<img src="{{asset('img/call.svg')}}">
						<a href="tel:{{$about->phone}}">{{$about->phone}}</a>
					</div>
					<div class="social_icons">

						<a href="{{$about->facebook}}" target="blank">
							<img src="{{asset('img/fb.svg')}}">
						</a>
						<a href="{{$about->telegram}}" target="blank">
							<img src="{{asset('img/telegram.svg')}}">
						</a>
						<a href="{{$about->instagram}}" target="blank">
							<img src="{{asset('img/instagram.svg')}}">
						</a>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="courses_list">
						<a href="javascript:void(0);">{{__('message.courses_english3')}}</a>
						<a href="javascript:void(0);">{{__('message.courses_english3')}}</a>
						<a href="javascript:void(0);">City Speaking</a>
						<a href="javascript:void(0);">City Kids</a>
						<a href="javascript:void(0);">IELTS</a>
						<a href="javascript:void(0);">CEFR</a>
						<a href="javascript:void(0);">{{__('message.courses_corporate')}}</a>
					</div>
				</div>	
				<div class="col-md-4 col-sm-6 col-sm logo_block">
					<img src="{{asset('img/city_white.svg')}}">
				</div>

			</div>
			<div class="footer_row">
				<a href="http://web-horizon.ru" target="blank">Сайт разработан студией Web Horizon</a>
			</div>
		</div>
	</footer>

	@include('partials._modals')


	<script src="{{asset('js/libs.min.js')}}"></script>
	<script src="{{asset('js/script.js')}}"></script>
	<script src="https://cdn.jsdelivr.net/npm/lozad/dist/lozad.min.js"></script>
	<script type="text/javascript">

		$("form").not('#contest_form').submit(function(event) {
			event.preventDefault();
			var data = {
				category: $(this).find(".category").val(),
				name: $(this).find("input[name=name]").val(),
				number: $(this).find("input[name=phone]").val(),
				mail: $(this).find("input[name=mail]").val(),
				pupils_amount: $(this).find("input[name=pupils_amount]").val(),
				lesson_amount: $(this).find("input[name=lesson_amount]").val(),
				msg: $(this).find("textarea[name=msg]").val(),
				status: $(this).find("select[name=status]").val(),
				date: $(this).find("input[name=date]").val()
			};

			console.log(data);

			$.ajax({
				type: "POST",
				url: 'send.php',
				data: data,
				success: function(data) {
					console.log(data);
					alert('Ваша заявка успешно принята!');
				}
			}); 
		});
		$("#contest_form").submit(function(event) {
			event.preventDefault();
			var data = {
		  		name: $(this).find("input[name=name]").val(),
		  		surname: $(this).find("input[name=surname]").val(),
		  		phone: $(this).find("input[name=phone]").val(),
		  		time: $(this).find("input[name=time]").val(),
		  		courses: $(this).find("input[name=courses]").val()
		  	};

		  	console.log(data);

		 	$.ajax({
			  	type: "POST",
				url: 'db.php',
			  	data: data,
			  	success: function(data) {
			  		console.log(data);
			  		alert('Ваша заявка успешно принята!');
			  	}
		 	}); 
		});
		const observer = lozad();
		observer.observe();
	</script>
</body>
</html>