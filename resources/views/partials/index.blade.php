@extends('layouts.app')

@section('title', 'Английский язык || Курсы английского языка в Ташкенте || Английский за 3 месяца!')
@section('description', 'Приходите на курсы английского языка в City Education и освойте английский язык всего лишь за 3 месяца!')
@section('keywords', 'Курсы английского языка')

@section('content')

@include('partials._slider')


@if($courses)

@include('partials._course', ['locale' => $locale, 'courses' => $courses])

@endif

@include('partials._teacher', ['locale' => $locale, 'teachers' => $teachers])

@include('partials._features')

@include('partials._statistic', ['locale' => $locale, 'statistika' => $statistika])
<!-- @include('partials._gallery', ['locale' => $locale, 'galery' => $galery])
 -->
@include('partials._news', ['locale' => $locale, 'news' => $news])

@include('partials._review', ['locale' => $locale, 'reviews' => $reviews])

@include('partials._about', ['locale' => $locale, 'about' => $about, 'partners' => $partners])

@include('partials._branch', ['locale' => $locale, 'branches' => $branches])

<div class="modal sign_in_modal">
	<div class="close_modal">
		<img src="img/cancel.svg">
	</div>
	<div class="modal_content modal_contest">
		<div class="contest_block">
			<div class="contest_desc">
				<img src="img/banner.png" class="main_img">
				<h3>Запишитесь на пробный урок!</h3>
				<a href="#" class="contest_btn">Записаться!</a>
				<p>Всем тем кто хочет изучить английский за короткий срок!<br><br>
				У нас для вас отличное предложение!<br><br>
				Зарегистрируйтесь на любой наш курс по английскому языку, и вы получите возможность обучаться бесплатно в следующем месяце. Здесь все зависит только от ВАС и ваших стараний.<br><br>
				Наши условия: После окончания кождого месяца обучения вы сдадите экзамен на освоенный материал. ЕСЛИ вы наберете больше 90 баллов, вы проходите следующий месяц обучения абсолютно бесплатно ⬅️.  Для вас это отличный мотиватор чтобы изучать английский в полную силу💪💪💪.
				<br><br>
				Для начала вы можете зарегистрироваться на наш пробный урок у нас на сайте(ссылка в био) или же по номеру <a href="tel:+998712003030">+998 71 200 30 30</a>.
				<br><br>
				Подписывайтесь на нашу <a href="http://instagram.com/cityuz_official" target="blank">страничку</a>, следите за нами, ставьте лайки, мы вам всегда будем рады!
				<br>
				______________
				<br><br>
				Salom do'stlar!
				Sizlarga ucun juda zor taklif bor:
				Bizning ingliz tili kurslarimizga obuna bo'ling va kelasi oydan bepul o'qing, bu yerda hammasi faqatgina sizning harakatlaringizga bog'liq. 
				Bizning shartlarimiz: Oyning ohirida, sizga o'tilgan darslardan imtihon olinadi va undan 90dan yuqori ball toplasangiz, siz keyingi oy o'quv kurslarini bepul o'qiysiz!
				<br><br>
				Bizning websaytimizdan yoki +998 71 200 30 30 raqamga qong'iroq qiling va bepul darsga obuna bo'ling!
				<br><br>
				Bizni sahifamizga a'zo bo'ling va bizni kuzatib boring! Biz bundan hursand bo'lamiz!
				<br><br>
				P.S. Tez oradan butunlay bepul o'qish uchun konkurs o'tkazishni rejalashtirmoqdamiz. Sizda esa ushbu imkoniyatni qo'lga kiritish imkoniyati bo'ladi.
				<br><br>
				Have a good time, my friends😘</p>
			</div>
			<div class="contest_form">
				<form method="POST" id="contest_form">
					<div class="back_btn">
						<svg class="flickity-button-icon" viewBox="0 0 100 100"><path d="M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z" class="arrow"></path></svg>
						<span>Назад</span>
					</div>
					<h4>Оставьте заявку на пробный урок!</h4>
					
					<div class="input_block">
						<p>Ism / имя</p>
						<input type="text" name="name" required="">
					</div>
					<div class="input_block">
						<p>Familiya / фамилия</p>
						<input type="text" name="surname" required="">
					</div>
					<div class="input_block">
						<p>Telefon raqamingiz / номер телефона</p>
						<input type="text" name="phone" value="998" pattern=".{14,14}" required="">
					</div>
					<div class="input_block">
						<p>Siz taxsil olmoqchi bo'lgan kurs / Курс, в который вы хотите записаться</p>
						<div class="radio">
							<input type="radio" value="Английский за 3 месяца" name="courses" id="6" required="">
							<label for="6">Английский за 3 месяца</label>
						</div>
						<div class="radio">
							<input type="radio" value="Английский за 6 месяцев" name="courses" id="7">
							<label for="7">Английский за 6 месяцев</label>
						</div>
						<div class="radio">
							<input type="radio" value="CEFR" name="courses" id="8">
							<label for="8">CEFR</label>
						</div>
						<div class="radio">
							<input type="radio" value="IELTS" name="courses" id="9">
							<label for="9">IELTS</label>
						</div>
					</div>
					
					<div class="input_block time_block">
						<p>Siz uchun qulay vaqt / Подходящее для вас время</p>
						
						<div class="radio">
							<input type="radio" value="09:00" name="time" id="1" required="" checked="">
							<label for="1" >09:00</label>
						</div>
						<div class="radio">
							<input type="radio" value="12:00" name="time" id="2">
							<label for="2">12:00</label>
						</div>
						<div class="radio">
							<input type="radio" value="15:00" name="time" id="3">
							<label for="3">15:00</label>
						</div>
						<div class="radio">
							<input type="radio" value="18:00" name="time" id="4">
							<label for="4">18:00</label>
						</div>
						

					</div>
					
					<div class="input_block submit_block">
						<input type="submit" value="Отправить" name="submit">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection