@extends('layouts.app')

@section('content')

<section class="breadcumbs_section">
    <div class="container">
        <div class="row">
            <div class="breadcumb_text">
                <h1>{{__('message.news_title')}}</h1>
                <div class="links">
                    <a href="{{route('home', ['locale' => $locale])}}">{{__('message.main')}} <span>/</span></a>
                    <a href="{{route('news', ['locale' => $locale])}}"> {{__('message.news_title')}}<span>/</span></a>
                    <span>{{substr($newsItem->getTranslatedAttribute('title', $locale, 'ru'), 0, 30)}}...</span>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="post_section">
    <div class="container">
        <div class="row">
            <div class="post_block col-md-8">
                <h1>{{$newsItem->getTranslatedAttribute('title', $locale, 'ru')}}</h1>
                <h4>{{date('d.m.Y', strtotime($newsItem->date))}}</h4>
                <img src="{{asset(($newsItem->image)?'storage/'. $newsItem->image:'img/no-photo.png')}}">
                <div class="short_desc">
                    {{$newsItem->getTranslatedAttribute('short_desc', $locale, 'ru')}}
                </div>
				{!!$newsItem->getTranslatedAttribute('desc', $locale, 'ru')!!}
            </div>
            <div class="news_list col-md-4">
				@if(!empty($news))

                @foreach($news as $key => $item)

				<div class="news_list_block">
                    <h4>Вам также может быть интересно:</h4>
                    <div class="news_block">
                        <a href="#" class="news_item">
                            <img src="{{asset(($newsItem->image)?'storage/'. $newsItem->image:'img/no-photo.png')}}">
                            <div class="news_desc">
                                <h1>{{substr($newsItem->getTranslatedAttribute('title', $locale, 'ru'), 0, 30)}}...</h1>
                                <h5>{{date('d.m.Y', strtotime($newsItem->date))}}</h5>
                                <div class="text">
                                    {!! $newsItem->getTranslatedAttribute('short_desc', $locale, 'ru')!!}
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

				@endforeach

				@endif

            </div>
        </div>
    </div>
</section>
@endsection