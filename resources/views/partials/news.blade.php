@extends('layouts.app')

@section('title', 'Новости')

@section('content')

<section class="breadcumbs_section">
    <div class="container">
        <div class="row">
            <div class="breadcumb_text">
                <h1>Новости</h1>
                <div class="links">
                    <a href="{{route('home', ['locale' => $locale])}}">Главная <span>/</span></a>
                    <span> Новости</span>
                </div>
            </div>
        </div>
    </div>
</section>


@if(!empty($news))
<section class="news_section">
    <div class="container">
        <div class="section_title">
            <h1>Новости</h1>
            <p>Фотографии с мероприятий</p>
        </div>
        <div class="row">

        	@foreach($news as $key => $item)

            @include('partials._news_item', ['locale' => $locale, 'item' => $item])

            @endforeach

        </div>
	    {{$news->links()}}
    </div>
</section>
@endif

@endsection