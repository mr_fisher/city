@extends('layouts.app')

@section('title', $courseItem->getTranslatedAttribute('title', $locale, 'ru'))
@section('description', "Курсы IELTS в Ташкенте, CEFR в Ташкенте, разговорный английский, английский язык для детей в Ташкенте, Ingliz tili o'quv markazi")

@section('content')
<section class="breadcumbs_section courses_breadcumbs">
    <div class="container">
        <div class="row">
            <div class="breadcumb_text">
                <h1>{{$courseItem->getTranslatedAttribute('title', $locale, 'ru')}}</h1>
                <div class="links">
                    <a href="{{route('home', ['locale' => $locale])}}">{{__('message.main')}} <span>/</span></a>
                    <span>{{$courseItem->getTranslatedAttribute('title', $locale, 'ru')}}</span>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="course_section">
	<div class="container">
		<div class="row">
			<div class="course_img">
			<img src="{{asset(($courseItem->image)?'storage/'. $courseItem->image:'img/no-photo.png')}}">
			</div>
			<div class="course_desc">
				<h2 class="course_title">{{$courseItem->getTranslatedAttribute('title', $locale, 'ru')}}</h2>
				<div class="list">
					<div class="list_item">{{__('message.courses_duration')}} : {{$courseItem->getTranslatedAttribute('duration_of_course', $locale, 'ru')}}</div>
					<div class="list_item">{{__('message.courses_week')}} : {{$courseItem->getTranslatedAttribute('number_of_lessons', $locale, 'ru')}}</div>
					<div class="list_item">{{__('message.courses_time')}} : {{$courseItem->getTranslatedAttribute('duration_of_lesson', $locale, 'ru')}}</div>
					<!-- <div class="list_item">График: выбираете сами(с 09:00 до 21:00)</div> -->
				</div>
				<div class="btns">
					<a href="#form" >{{__('message.rollin')}}</a>
				</div>	
			</div>
			<div class="course_text">
				{!!$courseItem->getTranslatedAttribute('desc', $locale, 'ru')!!}
				<div class="btns">
					<a href="#">{{__('message.rollin')}}</a>
				</div>	
			</div>
			<div class="link">
				<a href="{{route('home', ['locale' => $locale])}}">&#8592; {{__('message.back')}}</a>
			</div>
		</div>
	</div>
</section>

<div class="modal sign_in_modal" style="display: block;">
  <div class="close_modal">
    <img src="{{asset('img/cancel.svg')}}">
  </div>
  <div class="modal_content modal_contest">
    <div class="contest_block">
	      
	      <div class="contest_form">
	        <form id="contest_form" method="POST" action="{{route('book')}}">
					@csrf
					<input type="hidden" name="course" value="{{$courseItem->getTranslatedAttribute('title', $locale, 'ru')}}">
					<input type="hidden" name="type" value="1">
	          
	          <h4>{{__('message.rollin_title')}}</h4>
	        
	          <div class="input_block">
	            <p>{{__('message.rollin_name')}}</p>
	            <input type="text" name="first_name" required>
	          </div>
	          <div class="input_block">
	            <p>{{__('message.rollin_surname')}}</p>
	            <input type="text" name="last_name" required>
	          </div>
	          <div class="input_block">
	            <p>{{__('message.rollin_number')}}</p>
	            <input type="text" name="phone" value="998" pattern=".{14,14}" required>
	          </div>
	          
	          <div class="input_block">
	            <p>{{__('message.type_callback')}}</p>
	            <div class="radio">
	              <input type="radio" value="Позвонить вам" name="callback_type" id="6" required="" checked>
	              <label for="6">{{__('message.rollin_call')}}</label>
	            </div>
	            <div class="radio">
	              <input type="radio" value="Написать вам в Telegram" name="callback_type" id="7">
	              <label for="7">{{__('message.rollin_telegram')}}</label>
	            </div>
	          </div>

	          <div class="input_block">
  	          <p>{{__('message.rollin_region')}}</p>

	            <select>
            		<option selected>{{__('message.region')}}</option>
            		<option>{{__('message.region2')}}</option>
            		<option>{{__('message.region3')}}</option>
            		<option>{{__('message.region4')}}</option>
            		<option>{{__('message.region5')}}</option>
            		<option>{{__('message.region6')}}</option>
            		<option>{{__('message.region7')}}</option>
            		<option>{{__('message.region8')}}</option>
            		<option>{{__('message.region9')}}</option>
            		<option>{{__('message.region10')}}</option>
            		<option>{{__('message.region11')}}</option>
            		<option>{{__('message.region12')}}</option>
            		<option>{{__('message.region13')}}</option>
            		<option>{{__('message.region14')}}</option>
            		
            	</select>
	          </div>
	          
	          <div class="input_block time_block">
	            <p>{{__('message.rollin_time')}}</p>
	            
	            <div class="radio">
	              <input type="radio" value="09:00" name="time" id="1" required>
	              <label for="1">09:00</label>
	            </div>
	            <div class="radio radio_12">
	              <input type="radio" value="12:00" name="time" id="2">
	              <label for="2">12:00</label>
	            </div>
	            <div class="radio">
	              <input type="radio" value="15:00" name="time" id="3">
	              <label for="3">15:00</label>
	            </div>
	            <div class="radio radio_18">
	              <input type="radio" value="18:00" name="time" id="4">
	              <label for="4">18:00</label>
	            </div>
	          </div>
	          <div class="input_block submit_block">
	          	<button type="submit">{{__('message.send_btn')}}</button>
	          </div>
	        </form>
	      </div>
    </div>
  </div>
</div>

@endsection

@section('scripts')

@stop

