<?php

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::post('/book', 'HomeController@book')->name('book');

Route::get('{locale}/gallery/{id}', 'HomeController@gallery')->name('gallery');

Route::get('/', 'HomeController@index')->name('home');

Route::get('{locale}', 'HomeController@index')->name('home');

Route::get('{locale}/news', 'HomeController@news')->name('news');

Route::get('{locale}/list', 'HomeController@list')->name('list');

Route::get('{locale}/news/{url}', 'HomeController@newsDetail')->name('newsDetail');

Route::get('{locale}/course/{url}', 'HomeController@courseDetail')->name('courseDetail');

Route::get('{locale}/registraciya/{url}', 'HomeController@registraciya')->name('registraciya');




//adasdasdad
